# Metazoa Changelog

## In-progress Changes

 * Added `glossa.metazoa.api.effects` for annotating function effect types, taken from [Koka](https://koka-lang.org).
 * Changed unary `glossa.metazoa.view` to view an IMeta's `glossa.metazoa/doc` value by default, rather than return the sorted set of metadata providers.
   * Added `glossa.metazoa/providers` to provide same value that unary `meta/view` provided before.
