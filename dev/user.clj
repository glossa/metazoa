(ns user
  (:require
   [clojure.string :as str]
   [datascript.core :as d]
   [glossa.metazoa :as meta]
   [glossa.metazoa-meta]
   [glossa.metazoa.optional-deps :as deps]
   [glossa.metazoa.provider.tutorial :as tutorial]
   [glossa.metazoa.util :as util]
   [sc.api]
   [glossa.metazoa.api :as api]))

(def core-namespaces
  (meta/query
    '[:find [?imeta ...]
      :in $ package? ?klass
      :where
      [?e :imeta/type ?klass]
      [?e :imeta/this ?imeta]
      [(clojure.core/ns-name ?imeta) ?ns-name]
      [(package? ?ns-name)]]
    (fn package?
      [ns]
      (let [n (ns-name ns)]
        (and (str/starts-with? n "clojure.")
             (not (str/starts-with? n "clojure.tools"))
             (not (str/starts-with? n "clojure.spec")))))
    clojure.lang.Namespace))

(comment
  (meta/help)

  (meta/view (the-ns 'glossa.metazoa) :glossa.metazoa/doc)
  (meta/view (the-ns 'glossa.metazoa))

  (meta/view #'max ::meta/fn-table)

  (meta/reset-query)
  (meta/query '[:find ?imeta .
                :where
                [?e ::meta/tutorial _]
                [?e :ns ?ns]
                [?e :imeta/this ?imeta]
                [?e :name ?name]])

  ;; 220 with macros
  ;; 137 just normal functions
  (sort-by
    (comp :name meta)
    (meta/query '[:find [?imeta ...]
                  :in $ var-args? [?ns ...]
                  :where
                  [?e :arglists ?arglists]
                  [(var-args? ?arglists)]
                  [?e :ns ?ns]
                  (not [?e :macro])
                  [?e :imeta/this ?imeta]]
                (fn var-args?
                  [arglists]
                  (some (fn [lst] (some #{'&} lst)) arglists))
                core-namespaces))

  (doseq [vr (sort-by
               (comp :name meta)
               (meta/query '[:find [?imeta ...]
                             :in $ var-args? [?ns ...]
                             :where
                             [?e :arglists ?arglists]
                             [(var-args? ?arglists)]
                             [?e :ns ?ns]
                             (not [?e :macro])
                             [?e :imeta/this ?imeta]]
                           (fn nullary?
                             [arglists]
                             (some empty? arglists))
                           core-namespaces))]
    (println "|" vr "|"))

  (meta/view #'meta/view ::meta/tutorial)

  (->> (meta/query '[:find ?imeta .
                     :where
                     [?imeta ::meta/namespaces _]])
       (d/entity (meta/db))
       (keys))

  (meta/reset-search)
  (meta/search "source")
  (meta/search {:query "source"
                :num-hits 10})
  (meta/search 'source)
  (meta/search 42) ; invalid query
  (meta/search "name:source AND macro:true")
  (meta/search "name:source AND -macro")
  (meta/search "ns:clojure.core AND value-type:java.lang.String")

  (sc.api/dispose-all!)
  (sc.api/last-ep-id)

  (meta/view (the-ns 'glossa.metazoa) ::meta/tutorial)
  tutorial/session
  (tutorial/|>|)
  (tutorial/|<|)
  (tutorial/|history|)
  (tutorial/|>>|)
  (tutorial/|<<|)

  (mapcat keys deps/recommendations)

  (count (d/datoms (meta/db) :eavt))

  (def ana (api/find-cached-analysis))

  (meta/query '[:find ?ns ?name
                :in $ ?this
                :where
                [?callee :imeta/this ?this]
                [?var-usage :var-usage/ref-to-var ?callee]
                [?var-usage :var-usage/ref-from-var ?caller]
                [?caller :ns ?ns]
                [?caller :name ?name]]
              #'util/full-name)

  )

