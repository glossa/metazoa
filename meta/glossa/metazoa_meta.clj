#_{:clj-kondo/ignore [:unused-namespace]}
(ns glossa.metazoa-meta
  "Meta namespace for glossa.metazoa

  Equivalent to -test but for annotations."
  (:require
   [clojure.pprint :as pprint]
   [clojure.string :as str]
   [glossa.metazoa :as meta]
   [glossa.metazoa.api :as meta.api]
   [glossa.metazoa.optional-deps :as deps]
   [glossa.metazoa.query :as query]
   [glossa.metazoa.search :as search]
   [glossa.metazoa.util :as util]
   [glossa.weave :as weave]))

;; Metadata on Clojure vars, used in the README and tutorial.

(alter-meta!
 #'clojure.core/get
 assoc
 ::meta/fn-table
 {:left-hand-args [nil {:a "alpha"}]
  :right-hand-args [[:a] [:b "default"]]})

(alter-meta!
 #'clojure.core/max
 assoc
 ::meta/fn-table
 {:left-hand-args  [0 1]
  :right-hand-args [0 1]
  :label           'OR}
 ::meta/example
 {:code '(max 5 -5 10 0)
  :expected 10
  :ns *ns*})

(def and-fn-table
  {:left-hand-args  [0 1]
   :right-hand-args [0 1]
   :label           'AND
   :function        min})

(alter-meta!
 #'clojure.core/min
 assoc
 ::meta/fn-table
 and-fn-table
 ::meta/doc
 [:div
  [:p "Like many Clojure math functions, `min` takes a variable number of arguments:"]
  [::meta/example
   {:code '(min 5 -5 10 0)
    :expected -5
    :ns *ns*}]
  [:p "Using only 0 and 1, the `min` function models logical AND:"]
  [::meta/fn-table and-fn-table]])

(alter-meta!
 #'clojure.core/name
 assoc
 ::meta/example
 [{:label "The `name` function generously converts unqualified symbols, keywords, and strings to strings."
   :code '(= (name 'alpha) (name :alpha) (name "alpha"))
   :ns *ns*}
  {:label "If a qualified symbol or keyword, `name` returns the second part, without the namespace included."
   :code '(= "bar" (name 'foo/bar) (name :foo/bar))
   :expected true
   :ns *ns*}])

;; The README

(defn with-ns*
  [ns coll]
  (mapv #(assoc % :ns ns) coll))

(def with-ns (partial with-ns* *ns*))

(def ^:private readme|viewing
  [:div
   [:p "The " [:code-clj 'meta/view] " function has been designed for REPL use, printing to " [:code-clj '*out*] " with leading semicolons and a narrow column width. Review the following examples to gain an intuition of how " [:code-clj 'meta/view] " works:"]
   [::meta/example
    (with-ns
      [{:label "What metadata providers are available on this namespace?"
        :code "(meta/providers 'glossa.metazoa)"
        :expected (sort [:glossa.metazoa/tutorial :glossa.metazoa/doc])}
       {:label "View a var's example"
        :code "(meta/view #'clojure.core/name ::meta/example)"}
       {:label "View a function table"
        :code "(meta/view #'clojure.core/max ::meta/fn-table)"}
       {:label "View a standalone metadata provider value, useful while developing it"
        :code '(meta/view (meta/example {:ns *ns* :code '(map max [0 0 1 1] [0 1 0 1])}))}])]
   [:p "The" [:code-clj 'meta/view] " function will attempt to resolve a symbol to a namespace or var (as seen in the first example)."]
   [:p "While printing returns " [:code-clj 'nil] " in Clojure, the " [:code-clj 'meta/view] " function returns the given IMeta, so that you can thread calls to " [:code-clj 'meta/view] "and other Metazoa functions that take an IMeta instance."]
   [:p "There are two multimethods that underlie " [:code-clj 'meta/view] ":"]
   [:ul
    [:li [:code-clj 'meta.api/render-metadata] " Return a value that can be trivially printed."]
    [:li [:code-clj 'meta.api/view-metadata] " Provide a custom view experience beyond simple printing."]]
   [:p "Most metadata providers need only implement " [:code-clj 'meta.api/render-metadata] ", because " [:code-clj 'meta/view] " prints its return value if a separate " [:code-clj 'meta/view-metadata] " implementation is not found. The " [:code-clj ::meta/tutorial] " provider implements its own " [:code-clj 'meta/view-metadata] " to provide an interactive, in-REPL tutorial player."]])

(def ^:private readme|searching
  [:div
   [:p [:strong "Optional Dependencies,"] " included by default:"]
   [:ul
    [:li [:a {:href "https://lucene.apache.org/"} "Apache Lucene"] ", latest tested artifacts:"
     (into [:ul]
           (->> (sort deps/recommended-lucene-artifacts)
                (map (fn [[artifact version]]
                       [:li [:code (str artifact " {:mvn/version \"" (:mvn/version version) "\"}")]]))))]]

   [:p "Metazoa's " [:code-clj 'meta/search] " function allows you to search your code base's metadata using Lucene queries:"]
   [::meta/example
    (with-ns
      [{:label "Search for 'source'"
        :code '(take 5 (meta/search "source"))
        :pretty-result true}
       {:label "How many results was that?"
        :code '(count (meta/search "source"))}
       {:label (str "If " search/default-num-hits ", that's the default limit. How many really?")
        :code '(:total-hits (meta (meta/search "source")))}
       {:label "You can specify `:num-hits` or `:limit`"
        :code '(meta/search {:query "source" :num-hits 3})}
       {:label "Exclude certain namespace patterns"
        :code '(count (meta/search "name:source AND -ns:cider.*"))}
       {:label "Limit results to macros"
        :code '(meta/search "name:source AND -ns:cider.* AND macro:true")}
       {:label "How many public forms are in namespaces prefixed with 'clojure.' ?"
        :code '(-> (meta/search "ns:clojure.*")
                   meta
                   :total-hits)}
       {:label "Which namespaces does that include?"
        :code '(meta/search "id:clojure.* AND imeta-type:clojure.lang.Namespace")
        :pretty-result true}])]
   [:p "Note: Results are limited to " search/default-num-hits " hits by default. Use the map-based query and specify `:num-hits` or `:limit` (see examples above) to adjust this."]
   [:p "In the first example, how did it match the " [:em "namespace"] " " [:code-clj 'glossa.metazoa] "? All of your code base's metadata is indexed, not just names of vars and namespaces. For Clojure vars, Metazoa's search indexing includes only " [:em "public"] " ones by default, but you have the option to provide a collection of IMetas to be indexed yourself as follows:"]
   [::meta/example
    [{:ns *ns*
      :label "This indexes _all_ vars, not just public ones:"
      :eval? false
      :code '(meta/reset-search (meta.api/find-imetas (fn [ns] (conj ((comp vals ns-interns) ns) ns))))}]]
   [:p "The metadata map of each IMeta in your code is indexed as a separate Lucene document. Each metadata entry of each IMeta is indexed as a separate field in those Lucene documents. By default, string metadata values are indexed as full text fields and most others are indexed as simple text fields (using " [:code-clj 'str] " of the value). Every Lucene document has a " [:code "imeta-symbol"] " and " [:code "imeta-type"] " fields, which are the fully-qualified identifier and the type of the source IMeta, as well as a " [:code "imeta-value-type"] " field which is the type of the underlying value contained by an IMeta that is a Clojure var."]

   [:p "All fully-qualified idents have their " [:code "/"] " character replaced with " [:code "_"] " to be acceptable for Lucene query syntax. If a document fails to index, a message is printed to " [:code-clj '*err*] " but the indexing process will proceed to index as many IMetas as possible."]

   [:p "Metadata providers can implement custom search indexing behavior. To customize how your metadata is indexed, implement the " [:code-clj 'meta.api/index-for-search] " multimethod and return a map as follows:"]
   [::meta/example
    (with-ns
      [{:label "Option A: Supply a function expecting a Lucene Document, use Lucene API to index your field."
        :eval? false
        :code (with-out-str
                (pprint/pprint
                 '{:lucene
                   {:index-fn
                    (fn [doc] (.add doc (TextField. "your-field" "your-value" Field$Store/NO)))}}))}
       {:label (str "Option B: Supply a map specifying how to index your metadata value."
                    "          Currently limited in expressivity.")
        :eval? false
        :code (with-out-str
                (pprint/pprint {:lucene
                                {:field :text-field
                                 :stored? false
                                 :value "Some custom stringification of your metadata value."}}))}])]])

(def ^:private readme|querying
  [:div
   [:p [:strong "Optional Dependencies,"] " included by default:"]
   [:ul
    [:li [:a {:href "https://github.com/tonsky/datascript"} "DataScript"] ", latest tested artifacts:"
     (into [:ul]
           (->> (sort deps/recommended-datascript-artifacts)
                (map (fn [[artifact version]]
                       [:li [:code (str artifact " {:mvn/version \"" (:mvn/version version) "\"}")]]))))]]

   [:p "Metazoa's " [:code-clj 'meta/query] " function allows you to query your code base's metadata using Datalog queries:"]
   [::meta/example
    (with-ns
      [{:label "What was added to Clojure core in version 1.4?"
        :format true
        :code "(sort
                 (meta/query
                   '[:find [?name ...]
                     :in $ ?ns ?added
                     :where
                     [?e :ns ?ns]
                     [?e :name ?name]
                     [?e :added ?added]]
                   (the-ns 'clojure.core)
                   \"1.4\"))"}
       {:label "How many public vars lack a :doc string?"
        :format true
        :code "(meta/query
                 '[:find [(count ?e)]
                   :where
                   [?e :ns]
                   (not [?e :doc])])"}
       {:label "How many of those are functions?"
        :format true
        :code "(meta/query
                 '[:find [(count ?e)]
                   :where
                   [?e :ns]
                   [?e :imeta/value ?value]
                   [(clojure.core/fn? ?value)]
                   (not [?e :doc])])"}
       {:label "What are the important magic numbers in my code base?"
        :format true
        :code "(meta/query
                 '[:find ?imeta ?value
                   :in $ package
                   :where
                   [?e :ns ?ns]
                   [(package ?ns)]
                   [?e :imeta/value ?value]
                   [(clojure.core/number? ?value)]
                   [?e :imeta/this ?imeta]]
                 (fn package [ns] (str/starts-with? (str (ns-name ns)) \"glossa.\")))"}])]
   [:p "The metadata map of each IMeta in your code is transacted to the DataScript database as an entity. The Metazoa library transacts " [:code-clj :imeta/this] ", " [:code-clj :imeta/symbol] ", and " [:code-clj :imeta/type] " attributes which store the IMeta object itself, the qualified symbol identifier of the IMeta, and the type of the IMeta, respectively. In addition, if the IMeta is a Clojure var, the underlying value and its type are transacted as " [:code-clj :imeta/value] " and " [:code-clj :imeta/value-type] "."]])

(def ^:private readme|checking-testing
  [:div
   [:p "If you've taken the time to adorn your codebase with rich metadata, you should take the time to ensure it remains up-to-date. Metazoa supports this through its checking and testing story."]
   [::meta/example
    (with-ns
      [{:label "Add `:expected` to your ::meta/example metadata:"
        :code "(::meta/example (meta #'clojure.core/max))"}
       {:label "And you'll get meaningful check output:"
        :code "(meta/check #'clojure.core/max ::meta/example)"
        :format-result true
        :pretty-result true}])]
   [:p "Evaluate " [:code-clj '(meta/check imeta k)] " to see a data representation expressing your metadata's validity; run " [:code-clj '(meta/test-imeta imeta k)] " to assert the same validity using " [:code-clj 'clojure.test] ". The separation of functional checking and side-effecting testing allows users to wire up Metazoa with testing libraries other than " [:code-clj 'clojure.test] " if desired."]

   [:p "Both " [:code-clj 'meta/check] " and " [:code-clj 'meta/test-imeta] " will accept just an IMeta, in which case all of the metadata entries that have a " [:code-clj 'meta.api/check-metadata] " implementation will be exercised."]

   [:p "Run " [:code-clj '(meta/test-imetas)] " inside a " [:code-clj 'clojure.test/deftest] " form to test all metadata on all IMetas on the classpath."]])

(def ^:private readme|builtin-providers
  [:div
   [:p "For now, I suggest reading through the " [:code-clj 'glossa.metazoa-meta] " namespace to see Metazoa's metadata providers in action, including the source for the main tutorial and the source of this README document."]
   [:p "You can find Malli schemas under the " [:code-clj 'glossa.metazoa.provider] " namespaces that show what each provider expects."]
   [:p "The " [:code-clj "::meta/doc"] " provider expects a " [:a {:href "https://gitlab.com/glossa/weave"} "Weave"] " document value, extended to support nodes of " [:code-clj "::meta/example"] " and " [:code-clj "::meta/fn-table"] ", so that a single " [:code-clj "::meta/doc"] " can serve as a cohesive document with prose and testable code examples interwoven."]])

(def ^:private readme|optional-dependencies
  [:div
   [:p "Here are all of Metazoa's optional-but-included-by-default dependencies:"]
   (into [:ul]
         (->> (sort (mapcat identity deps/recommendations))
              (map (fn [[artifact version]]
                     [:li [:code (str artifact " {:mvn/version \"" (:mvn/version version) "\"}")]]))))
   [:p "You can exclude these via `:exclusions` in your project dependencies if you do not want to use the Metazoa features that rely on them."]
   [:p "Metazoa's searching and querying functions—since they are totally reliant on the third-party dependencies and are intended as interactive tools—will throw an exception if you attempt to use them without their dependencies. Code formatting and schema validation functionality supplied by cljfmt and malli respectively will simply be skipped without throwing exceptions."]])

(def ^:private readme|external-providers
  [:div
   [:p [:strong "Official:"]]
   [:ul
    [:li [:em "(Planned)"] " " [:code-clj ::meta/plantuml] " In " [:a {:href "https://plantuml.com/faq"} "its own words"] ": \"PlantUML is used to _draw_...diagrams, using a simple and human readable text description.\""]
    [:li [:em "(Planned)"] " " [:code-clj ::meta/vega-lite] " Create powerful visualizations of your data or the behavior of your functions using Vega-Lite as your \"high-level grammar of interactive graphics\""]]
   [:p [:strong "Community:"]]
   [:ul
    [:li "Please open an issue on this project and tag it with `community-metadata-provider` so it can be considered for inclusion here."]]])

(def ^:private readme|license
  [:div
   [:p "Copyright © Daniel Gregoire, 2021"]
   [:p "THE ACCOMPANYING PROGRAM IS PROVIDED UNDER THE TERMS OF THE [ECLIPSE PUBLIC LICENSE](/LICENSE) (\"AGREEMENT\"). ANY USE, REPRODUCTION OR DISTRIBUTION OF THE PROGRAM CONSTITUTES RECIPIENT'S ACCEPTANCE OF THIS AGREEMENT."]])

(defn readme
  "Why shouldn't the README of the project be metadata on the primary namespace?"
  ([] (readme {:version (System/getenv "METAZOA_VERSION") :sha (System/getenv "METAZOA_SHA") :tag (System/getenv "METAZOA_TAG")}))
  ([{:keys [version sha tag]
     :or {version "skip" sha "skip" tag "skip"}}]
   (let [viewing-header "Viewing"
         searching-header "Searching"
         querying-header "Querying"
         checking-header "Checking/Testing"
         builtin-providers-header "Built-in Providers"
         optional-dependencies-header "Optional Dependencies"
         external-providers-header "External Metadata Providers"
         license-header "License"
         header-fragment (fn [s] (str "#"
                                      (-> s
                                          (str/replace #"[\(\)/]" "")
                                          (str/replace #"\s" "-")
                                          (str/lower-case))))]
     [::weave/doc
      [:h1 "Metazoa"]
      [:p "The Metazoa library provides an extensible API for viewing, testing, searching, and querying Clojure metadata and includes several metadata providers that take full advantage of those tools: executable examples, function tables, interactive tutorials at the REPL, and structured documents that can contain other metadata provider values as nodes."]
      (when-not (= version "skip")
        [:pre {:lang "clj"}
         (str
          ";; Git dep\n"
          (format "{dev.glossa/metazoa\n {:git/url \"https://gitlab.com/glossa/metazoa.git\"\n  :git/tag \"%s\"\n  :git/sha \"%s\"}}"
                  tag sha)
          "\n\n;; Maven dep at https://clojars.org/dev.glossa/metazoa\n"
          (format "{dev.glossa/metazoa {:mvn/version \"%s\"}}"
                  version)
          "\n\n;; Exclusions of optional deps that are included by default:\n"
          ":exclusions " (pr-str (vec (sort (mapcat keys deps/recommendations)))))])
      [:p "Metazoa is best learned at the REPL:"]
      [:pre-clj "(require '[glossa.metazoa :as meta])\n(meta/help)"]
      [:p [:a {:href "https://www.youtube.com/watch?v=gSSh9srEE78"} "Introductory video on YouTube"]]
      [:p [:a {:href "https://www.danielgregoire.dev/posts/2021-10-15-clojure-src-test-meta/"} "Article on Motivation and Background"]]

      [:h2 "Introduction"]
      [:ul
       [:li [:a {:href (header-fragment viewing-header)} [:code-clj 'meta/view]] " View metadata in rich, interesting ways."]
       [:li [:a {:href (header-fragment checking-header)} [:code-clj 'meta/check]] " Check that your metadata is still valid via testing."]
       [:li [:a {:href (header-fragment searching-header)} [:code-clj 'meta/search]] " Search your project's metadata."]
       [:li [:a {:href (header-fragment querying-header)} [:code-clj 'meta/query]] " Query your project's metadata with Datalog queries."]]
      [:p "These functions rely on a set of multimethods defined in " [:code-clj '[glossa.metazoa.api :as meta.api]] " that form the " [:em [:strong "Metadata Provider API"]] ". A metadata provider implements one or more of these multimethods and can be identified by its dispatch value."]
      [:p "The built-in metadata providers are:"]
      (into [:ul]
            (->> (keys (methods meta.api/render-metadata))
                 (remove #(= % :default))
                 (sort)
                 (map #(vector :li [:a {:href (header-fragment builtin-providers-header)} [:code %]]))))

      [:p "If you have a REPL ready, try running " [:code-clj '(meta/help)] " to get started—there's an interactive tutorial waiting for you."]

      [:p "In all the sections that follow:"]
      [:ul
       [:li "IMeta refers to an instance of " [:code-clj 'clojure.lang.IMeta] ", which is a value that can store Clojure metadata."]
       [:li [:code ";; [out] "] " indicates characters printed via " [:code-clj '*out*]]
       [:li [:code ";; [err] "] " indicates characters printed via " [:code-clj '*err*]]
       [:li [:code "#_=> "] " indicates the evaluated value of a Clojure expression."]]

      [:h2 viewing-header]
      readme|viewing

      [:h2 checking-header]
      readme|checking-testing

      [:h2 searching-header]
      readme|searching

      [:h2 querying-header]
      readme|querying

      [:h2 builtin-providers-header]
      readme|builtin-providers

      [:h2 optional-dependencies-header]
      readme|optional-dependencies

      [:h2 external-providers-header]
      readme|external-providers

      [:h2 license-header]
      readme|license])))

#_(spit "README.md" (meta/render (the-ns 'glossa.metazoa) :glossa.metazoa/doc))

;; The Tutorial

(def tut-title
  "Metazoa Tutorial")

(def tut-0-intro
  {:content
   [:div
    [:h2 "Introduction"]
    [:p "Metazoa is a tool for viewing, searching, querying, and testing metadata."]
    [:p "This tutorial itself is stored as metadata on the " [:code-clj 'glossa.metazoa] " namespace:"]
    [::meta/example
     {:code '(sort (keys (meta (the-ns 'glossa.metazoa))))
      :ns *ns*}]
    [:p "After each tutorial step, tutorial controls are rendered that you can evaluate to navigate the tutorial."]
    [:p "Evaluate " [:code-clj '(|help|)] " for generic tutorial help."]
    [:p "Evaluate " [:code-clj '(|>|)] " to continue the tutorial."]]})

(def tut-1-view
  {:content
   [:div
    [:h2 "Viewing Metadata"]
    [:p "Try evaluating the following:"]
    [::meta/example
     (with-ns
       [{:label "What metadata providers are available for a given form?"
         :code '(meta/view #'clojure.core/max)
         :expected [::meta/example ::meta/fn-table]
         :eval? false}
        {:label "And there's something called a function table:"
         :code "(meta/view #'clojure.core/max ::meta/fn-table)"
         :eval? false}
        {:label "There's an executable example:"
         :code "(meta/view #'clojure.core/max ::meta/example)"
         :eval? false}])]
    [:p "You'll notice this tutorial renders as Markdown, complete with code blocks, but Clojure expressions aren't commented out. You can evaluate them directly or copy and paste them at the REPL prompt (if you have one). "]]})

(def tut-2-check
  {:content
   [:div
    [:h3 "Checking/Testing Metadata"]
    [:p "A metadata provider is denoted by a qualified keyword. If any of Metazoa's metadata multimethods are implemented for that keyword, it's a metadata provider."]
    [::meta/example
     {:label "What are all the metadata providers currently on the classpath?"
      :code '(sort (:all (meta.api/metadata-providers)))
      :ns *ns*}]
    [:p "Rich metadata should be tested to ensure it remains valid over time. Use " [:code-clj 'meta/check] " for a data structure describing whether a given metadata provider's data is valid; use " [:code-clj 'meta/test-imeta] " to exercise that same representation via `clojure.test`:"]
    [::meta/example
     (with-ns
       [{:label "What do we expect of our `max` executable example?"
         :format true
         :code "(->> (meta #'max)
                     ::meta/example
                     ((juxt :code :expected)))"}
        {:label "Does that expectation hold? See :expected and :actual"
         :code '(meta/check #'max ::meta/example)}])]]})

(def tut-3-doc
  {:content
   [:div
    [:h2 "Metazoa Documents"]
    [:p "The work-horse metadata provider is " [:code-clj ::meta/doc] " because it integrates the rendering and checking power of the other metadata providers."]
    [:p "A " [:code-clj ::meta/doc] " is a " [:a {:href "https://gitlab.com/glossa/weave"} "Weave"] " document. A Weave document is a Hiccup-style nested vector of vectors, supporting most HTML-like tag names. In Metazoa, there is also special support for " [:code-clj ::meta/example] " and " [:code-clj ::meta/fn-table] " nodes:"]
    [::meta/example
     (with-ns
       [{:code '(get (meta #'min) ::meta/doc)}
        {:label "Meta docs in my meta docs? Evaluate this:"
         :eval? false
         :code '(meta/view #'min ::meta/doc)}])]]})

(def tut-4-search
  {:content
   [:div
    [:h2 "Searching Metadata"]
    [:p "Metazoa provides both Lucene-backed full text search and Datascript-backed datalog querying to comb through your project's metadata. You can exclude these third-party dependencies if you only need Metazoa's view/check/test functionality."]
    [:p "Searching returns a sequence of " [:code-clj 'clojure.lang.IMeta] " instances, which are generally namespaces and public vars. You can index a custom set of IMetas, if needed."]
    [:p "Evaluate each example to view search results:"]
    [::meta/example
     (with-ns
       [{:label (str "Open-ended search across nearly all metadata fields, top " search/default-num-hits " hits by default:")
         :eval? false
         :code '(meta/search "source")}
        {:label "Specify :num-hits or :limit"
         :eval? false
         :code '(meta/search {:query "source" :num-hits 3})}
        {:label "Specify specific metadata fields to search:"
         :eval? false
         :code '(meta/search "name:source AND -ns:cider.* AND -macro")}
        {:label "Get all namespaces under a certain 'package':"
         :eval? false
         :code '(meta/search "id:clojure.* AND imeta-type:clojure.lang.Namespace")}])]
    [:p "As seen in the last example, Metazoa adds a few extra fields to each document in the search index:"]
    [:dl
     [:dt "imeta-symbol"] [:dd "The full symbol of the var or namespace."]
     [:dt "imeta-type"] [:dd "The type of the IMeta itself."]
     [:dt "imeta-value-type"] [:dd "The type of the _value_ contained by the var (if a var)."]]
    [:p "If your inquiry is more structural, prefer querying as explained in the next lesson."]]})

(def tut-5-query
  {:content
   [:div
    [:h2 "Querying Metadata"]
    [:p "Datascript is an in-memory database and Datalog query engine. Metazoa populates a Datalog database with tx-data based on IMetas and their metadata on the classpath."]
    [::meta/example
     (with-ns
       [{:label "What clojure.core forms were added in Clojure version 1.5 through 1.7?"
         :eval? false
         :format true
         :code "(sort
                  (meta/query
                    '[:find [?name ...]
                      :in $ ?ns [?added ...]
                      :where
                      [?e :ns ?ns]
                      [?e :name ?name]
                      [?e :added ?added]]
                    (the-ns 'clojure.core)
                    [\"1.5\" \"1.6\" \"1.7\"]))"}
        {:label "How many public vars lack a :doc string?"
         :eval? false
         :format true
         :code "(meta/query
                  '[:find [(count ?e)]
                    :where
                    [?e :ns]
                    (not [?e :doc])])"}
        {:label "IMetas with tutorials?"
         :eval? false
         :format true
         :code "(sort
                  (meta/query
                    '[:find [?imeta ...]
                      :where
                      [?e ::meta/tutorial]
                      [?e :imeta/this ?imeta]]))"}])]]})

(def tutorial
  {:title tut-title
   :author "Glossa Metazoa Maintainers"
   :ns *ns*
   :steps
   [[:intro tut-0-intro]
    [:viewing tut-1-view]
    [:checking tut-2-check]
    [:metazoa-doc tut-3-doc]
    [:searching tut-4-search]
    [:querying tut-5-query]]})

(alter-meta!
 (the-ns 'glossa.metazoa)
 assoc
 ::meta/doc (readme)
 ::meta/tutorial tutorial)
