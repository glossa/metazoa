#!/usr/bin/env bb
(ns todos
  (:require
    [babashka.fs :as fs]
    [babashka.process :refer [$]]
    [clojure.string :as str]))

(def file? (complement fs/directory?))

(def root (or (first *command-line-args*) (System/getProperty "user.dir")))
(def globs (let [rest-args (next *command-line-args*)]
             (if (seq rest-args)
               rest-args
               ["src/**" "test/**" "meta/**"])))

(def head-sha ((comp str/trim slurp :out) ($ git rev-parse HEAD)))
(def head-sha-prefix (subs head-sha 0 8))

(defn todo-link
  [file-path line-number]
  (let [url-format "https://gitlab.com/glossa/meta/-/blob/%s%s#L%s"
        url (format url-format head-sha file-path line-number)]
    (format "[TODO](%s)" url)))

(def section (atom nil))

(println "# TODOs\n")
(println "**As of commit:**" (format "[%s](https://gitlab.com/glossa/meta/-/tree/%s)" head-sha-prefix head-sha))
(doseq [path (mapcat (partial fs/glob root) globs)
        :when (file? path)
        :let [content (slurp (fs/file path))
              lines (str/split-lines content)
              relative-file-path (subs (.getAbsolutePath (.toFile path)) (count root))
              header (str "\n## " relative-file-path)]]
  (doseq [[idx line] (map-indexed #(vector %1 %2) lines)
          :let [match (re-find #"(?:(?:;+\s+)TODO\s+(.*))$" line)
                link (todo-link relative-file-path (inc idx))
                todo (second match)]
          :when todo]
    (when-not (= @section header)
      (println header)
      (println)
      (reset! section header))
    (println " -" link todo)))
