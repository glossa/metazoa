(ns ^{:glossa.metazoa/namespaces
      '[glossa.metazoa-meta]}
 glossa.metazoa
  "Tools for viewing, searching, querying, and testing Clojure metadata."
  (:require
   [clojure.pprint :as pprint]
   [clojure.set :as set]
   [clojure.test :as test]
   [glossa.metazoa.api :as api]
   [glossa.metazoa.fmt :as fmt]
   [glossa.metazoa.provider.doc :as doc]
   [glossa.metazoa.provider.example :as example]
   [glossa.metazoa.provider.fn-table :as fn-table]
   [glossa.metazoa.provider.tutorial :as tutorial]
   [glossa.metazoa.query :as query]
   [glossa.metazoa.search :as search]
   [glossa.metazoa.util :as util]))

;;
;; Implementations for render-metadata
;;

(defmethod api/render-metadata ::doc
  [imeta k]
  (doc/render (get (meta imeta) k)))

(defmethod api/render-metadata ::example
  [imeta k]
  (example/render (get (meta imeta) k)))

(defmethod api/render-metadata ::fn-table
  [imeta k]
  (let [meta-value (get (meta imeta) k)
        meta-value (cond-> meta-value
                     (not (:function meta-value)) (assoc :function imeta))]
    (fn-table/render meta-value)))

(defmethod api/render-metadata ::tutorial
  [_imeta _k]
  (throw (ex-info "Not implemented. Call `view-metadata` for an interactive tutorial."
                  {:error :not-implemented
                   :use-instead `api/view-metadata})))

;;
;; Implementations for view-metadata
;;

(defmethod api/view-metadata ::doc
  [imeta k]
  (let [rendered-doc (api/render-metadata imeta k)]
    (doc/view rendered-doc
              {:display-width 80
               :line-comment ";; "})))

(defmethod api/view-metadata ::tutorial
  [imeta k]
  (tutorial/view (get (meta imeta) k)))

;;
;; Implementations for check-metadata
;;

(defmethod api/check-metadata ::doc
  [imeta k]
  (doc/check (get (meta imeta) k)))

(defmethod api/check-metadata ::example
  [imeta k]
  (example/check (get (meta imeta) k)))

(defmethod api/check-metadata ::fn-table
  [imeta k]
  (let [meta-value (get (meta imeta) k)
        meta-value (cond-> meta-value
                     (not (:function meta-value)) (assoc :function imeta))]
    (fn-table/check meta-value)))

(defmethod api/check-metadata ::tutorial
  [imeta k]
  (tutorial/check (get (meta imeta) k)))

;;
;; Implementations for index-for-search
;;

(defmethod api/index-for-search ::doc
  [imeta k]
  (doc/index-for-search (get (meta imeta) k)))

;;
;; Implementations for query-schema and tx-data
;;

(comment

  (defmethod api/query-schema ::namespaces
    [_imeta _k]
    {::namespaces {:db/cardinality :db.cardinality/many
                   :db/valueType :db.type/ref
                   :db/doc "Namespaces responsible for applying metadata to this IMeta."}})

  (defmethod api/tx-data ::namespaces
    [_imeta _k]))

;;
;; Built-in Providers
;;

(defn doc
  "Use to annotate a metadata _value_ if you want a `::meta/doc` under an arbitrary key."
  [doc]
  (vary-meta doc assoc ::metadata-provider ::doc))

(defn example
  "Use to annotate a metadata _value_ if you want a `::meta/example` under an arbitrary key."
  [example]
  (vary-meta example assoc ::metadata-provider ::example))

(defn fn-table
  "Use to annotate a metadata _value_ if you want a `::meta/fn-table` under an arbitrary key."
  [fn-table]
  (vary-meta fn-table assoc ::metadata-provider ::fn-table))

(defn tutorial
  "Use to annotate a metadata _value_ if you want a `::meta/tutorial` under an arbitrary key."
  [tutorial]
  (vary-meta tutorial assoc ::metadata-provider ::tutorial))

(defn imeta
  "Use with meta/view, meta/check, etc., when authoring metadata and before it's been added to the final target IMeta."
  {::example
   [{:ns *ns*
     :code '(view (imeta {::example {:ns *ns* :code '(+ 1 2) :expected 3}}) ::example)
     :expected []}]}
  [metadata]
  (with-meta [] metadata))

;;
;; Public API
;;

(defn render
  [imeta k]
  (let [imeta (util/requiring-resolve+ imeta)]
    (util/load-namespaces imeta)
    (when (get (meta imeta) k)
      (api/render-metadata imeta k))))

(defn view
  ([imeta]
   (let [imeta (util/requiring-resolve+ imeta)
         _ (util/load-namespaces imeta)
         mm (meta imeta)]
     (if-let [metadata-provider (::metadata-provider mm)]
       (view (with-meta [] {metadata-provider imeta}) metadata-provider)
       (view imeta ::doc))))
  ([imeta k]
   (let [imeta (util/requiring-resolve+ imeta)]
     (util/load-namespaces imeta)
     (when (get (meta imeta) k)
       (println)
       (if (get (methods api/view-metadata) k)
         (api/view-metadata imeta k)
         (let [rendered (render imeta k)]
           (print rendered)
           imeta))))))

(defn check
  ([imeta]
   (let [imeta (util/requiring-resolve+ imeta)
         _ (util/load-namespaces imeta)
         mm (meta imeta)]
     (if-let [metadata-provider (::metadata-provider mm)]
       (check (with-meta [] {metadata-provider imeta}) metadata-provider)
       (reduce
        (fn [acc meta-key]
          (if-let [check-result (api/check-metadata imeta meta-key)]
            (assoc acc meta-key (util/ensure-sequential check-result))
            acc))
        {}
        (keys mm)))))
  ([imeta k]
   (let [imeta (util/requiring-resolve+ imeta)]
     (util/load-namespaces imeta)
     (when-let [check-result (api/check-metadata imeta k)]
       (util/ensure-sequential check-result)))))

(defn- test-check-result
  [imeta-name meta-key check-result]
  (when (seq check-result)
    (clojure.test/testing (str "Testing IMeta " imeta-name " at " meta-key ",")
      (doseq [[idx check] (map-indexed #(vector %1 %2) check-result)
              :let [{:keys [actual actual-err actual-out
                            expected expected-err expected-out
                            throwable]} check
                    explanation (pr-str check)]]
        (clojure.test/testing (pprint/cl-format nil "the ~:R item" idx)
          (cond
            (contains? check :throwable)
            (clojure.test/is (throw throwable))

            (contains? check :satisfies)
            (clojure.test/is (expected actual) (str "Your :satisfies predicate did not return a truthy value: " explanation))

            (contains? check :expected)
            (clojure.test/is (= expected actual) explanation)

            (contains? check :actual)
            (clojure.test/is true (str "Example evaluated without error to: " (pr-str actual)))

            :else
            (clojure.test/is nil (str "Your checks are malformed: " explanation)))
          (when (and expected-out actual-out)
            (clojure.test/is (= expected-out actual-out) "Unexpected *out*"))
          (when (and expected-err actual-err)
            (clojure.test/is (= expected-err actual-err) "Unexpected *err*")))))))

(defn test-imeta
  ([imeta k] (test-check-result (util/full-name imeta) k (check imeta k)))
  ([imeta]
   (doseq [[meta-key check-result] (check imeta)
           :let [imeta-name (util/full-name imeta)]]
     (test-check-result imeta-name meta-key check-result))))

(defn test-imetas
  ([] (test-imetas (api/find-imetas)))
  ([imetas]
   (doseq [imeta imetas]
     (test-imeta imeta))))

(defn search [search-query]
  (search/search search-query))

(defn reset-search
  ([] (search/reset))
  ([imetas] (search/reset imetas)))

(defn query
  [datalog-query & args]
  (apply query/q datalog-query args))

(defn reset-query
  ([] (query/reset))
  ([imetas] (query/reset imetas)))

(defn db [] (query/db))

(defn alter-metas!
  [irefs f & args]
  (doseq [iref irefs]
    (apply alter-meta! iref f args)))

(defn providers
  [imeta]
  (let [imeta (util/requiring-resolve+ imeta)
        _ (util/load-namespaces imeta)
        mm (meta imeta)]
    (sort (set/intersection (set (keys mm)) (set (:all (api/metadata-providers)))))))

(defn- usage-summary []
  (let [summary (doc/render
                 [:div
                  [:p "Welcome to Metazoa, where metadata comes to life!"]
                  [:p "Run this in your REPL if it's your first time:"]
                  [:pre-clj "(glossa.metazoa/view 'glossa.metazoa :glossa.metazoa/tutorial)"]])]
    (fmt/format-for-repl summary)))

(defn help
  [] (println (str "\n" (usage-summary))))
