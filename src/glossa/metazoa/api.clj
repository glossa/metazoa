(ns glossa.metazoa.api
  (:require
   [clojure.edn :as edn]
   [clojure.java.io :as io]
   [glossa.metazoa.optional-deps :as deps]))

(set! *warn-on-reflection* true)

(defn- dispatch
  [imeta k]
  (let [value (:glossa.metazoa/metadata-provider (meta (get (meta imeta) k)) k)]
    value))

(defmulti render-metadata #'dispatch)

(defmulti view-metadata #'dispatch)

(defmulti check-metadata #'dispatch)

(defmulti index-for-search #'dispatch)

(defmulti query-schema #'dispatch)

(defmulti tx-data #'dispatch)

;; Default defmethods

(defmethod render-metadata :default
  [imeta k]
  (get (meta imeta) k))

(defmethod check-metadata :default
  [_imeta _k]
  ;; NOTE: This is in support of calling check-metadata on all metadata entries of a given IMeta.
  nil)

(defmethod index-for-search :default
  [_ _]
  ;; See glossa.metazoa.search.lucene
  nil)

(defprotocol Indexable
  (db-id [this] "Produce an identifier (often a symbol) for `this` that is globally unique.")
  (doc-id [this] "Produce a string identifier for `this` that is globally unique, targeted at fulltext search indexing."))

(extend-protocol Indexable
  clojure.lang.Namespace
  (db-id [ns]
    (ns-name ns))
  (doc-id [ns] (str (db-id ns)))

  clojure.lang.Var
  (db-id [vr]
    (let [{var-ns :ns var-name :name} (meta vr)]
      (symbol (name (ns-name var-ns)) (name var-name))))
  (doc-id [vr] (str (db-id vr)))

  Object
  (db-id [o]
    (let [mm (meta o)]
      (if-let [id (or (get mm :glossa.metazoa/imeta)
                      (get mm :glossa.metazoa/id))]
        id
        (gensym (str (.getName (class o)) "_")))))
  (doc-id [o] (str (db-id o))))

(def schema-check-data
  '[:and
    [:map
     [:actual {:optional true} any?]
     [:actual-err {:optional true} string?]
     [:actual-out {:optional true} string?]
     [:expected {:optional true} any?]
     [:expected-err {:optional true} any?]
     [:expected-out {:optional true} any?]
     [:satisfies {:optional true} any?]
     [:throwable {:optional true}
      [:fn {:error/fn (fn [{:keys [_value]} _] "should be a java.lang.Throwable")}
       #(instance? Throwable %)]]]
    [:fn {:error/fn (fn [{:keys [_value]} _] "check must have either :actual or :throwable")}
     #(or (contains? % :actual) (contains? % :throwable))]])

(def schema-check (eval schema-check-data))

(def check-explainer
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/explainer) schema-check)))

(def check-validator
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/validator) schema-check)))

(defn metadata-providers
  []
  (let [render-providers (keys (dissoc (methods render-metadata) :default))
        view-providers (keys (dissoc (methods view-metadata) :default))
        check-providers (keys (dissoc (methods check-metadata) :default))]
    {:render render-providers
     :view view-providers
     :check check-providers
     :all (distinct (concat render-providers view-providers check-providers))}))

(defn find-imetas
  ([] (find-imetas (fn [ns] (conj ((comp vals ns-publics) ns) ns))))
  ([fn-find-imetas-for-ns]
   (persistent!
    (reduce
     (fn [acc ns]
       (let [imetas (conj (fn-find-imetas-for-ns ns))]
         (reduce #(conj! %1 %2) acc imetas)))
     (transient [])
     (all-ns)))))

(defn find-cached-analysis
  "At time of writing, this expects analysis as printed by clj-kondo"
  ([] (find-cached-analysis "analysis.edn"))
  ([analysis-edn-file]
   (let [file (io/file analysis-edn-file)]
     (when (.exists file)
       (binding [*read-eval* false]
         (:analysis (edn/read-string (slurp file))))))))

(def ^{:doc "Effects recognized by Metazoa. Taken from the Koka programming language."
       :glossa.metazoa/doc
       [:div
        [:h1 "Function Effects"]
        [:p "A mathematical function is a mapping from a domain to a range. Functions in computer programs are not so clean, but there is a spectrum of functional purity hygiene that is helpful to annotate in certain situations."]
        [:p "These effects are derived from the " [:a {:href "https://koka-lang.github.io/koka/doc/book.html#sec-effect-types"} "effect types"] " of the Koka programming language. These are intended _only_ for annotation; Metazoa does not perform any calculations or infer further information based on these effects."]
        [:p "Here is an explanation from the Koka documentation as to the meaning of these terms, and thus how they are intended to be used in Clojure metadata:"]
        [:blockquote
         "The absence of any effect is denoted as `total`...and corresponds to pure mathematical functions. If a function can raise an exception the effect is `exn`, and if a function may not terminate the effect is `div` (for divergence)...Non-deterministic functions get the `ndet` effect."]
        [:p "In addition to these, Metazoa includes a " [:code "console"] " effect for I/O that is limited to STDIN, STDOUT, and STDERR; " [:code "heap"] " for functions that directly require heap access; " [:code "mut"] " for code that mutates memory locations; " [:code "socket"] " for functions that communicate over sockets/networks; and finally " [:code "lazy"] " for functions whose values (and the effects that occur when materializing those values) are not realized at the time of function invocation. In Koka, some of the effects are aliases for multiple effects (e.g., Koka uses " [:code "pure"] " to signify a function that is both " [:code "exn"] " and " [:code "div"] "), but all of the effects listed here are indepenedent of one another."]
        [:p "Due to Clojure being a dynamically typed language, most functions are " [:code "exn"] " by default, unless the implementation explicitly catches all possible exceptions and returns a value instead. A " [:code "total"] " function would need to do that _and_ work only in values that do not require heap allocation of memory which, depending on what you want to convey with these annotations, would disqualify any function that even uses variable arity (since a collection is forged to hold the additional arguments)."]
        [:p "Obviously without effect type inference, any function that calls another function is a best guess as to the union of all effects in play. The intention of these annotations is to capture the function author's intent, not a semantic guarantee."]]}
  effects
  #{:total :heap :lazy :exn :div :ndet :console :socket})
