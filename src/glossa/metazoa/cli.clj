(ns glossa.metazoa.cli
  (:require
   [clojure.string :as str]
   [glossa.metazoa :as meta]
   [glossa.metazoa.util :refer [requiring-resolve+]]))

(defn- render-doc*
  [source source-key]
  (if source-key
    (meta/render source source-key)
    (meta/render source ::meta/doc)))

(defn render-doc
  [opts]
  (let [{:keys [source source-key target]} opts]
    (if-let [imeta (requiring-resolve+ source)]
      (let [doc-contents (render-doc* imeta source-key)]
        (if target
          (spit (str target) doc-contents)
          (println doc-contents)))
      (binding [*err* *out*]
        (println "[FAILED] Could not find:" source)))))

(defn help
  [_]
  (let [msg (str/join "\n"
                      ["Available functions:"
                       ""
                       "   - render-doc"
                       "      - clj -X glossa.metazoa.cli/render-doc :source glossa.metazoa :target README_gen.md"])]
    (println msg)))
