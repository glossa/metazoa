(ns glossa.metazoa.fmt
  (:require
   [clojure.string :as str]))

(set! *warn-on-reflection* true)

(defn format-table
  "Visually calmer version of clojure.pprint/print-table, returned as single string."
  [{:keys [header rows] :as table-spec}]
  (when-not (map? table-spec)
    (throw (ex-info "format-table excepts a map of :rows and optionally :header"
                    {:bad-table-spec table-spec})))
  (when (seq rows)
    (let [sb (StringBuilder.)
          rows (if header
                 (cons header rows)
                 rows)
          widths (reduce
                  (fn [acc row]
                    (map (fn [x y] (max x (count (pr-str y)))) acc row))
                  (map (comp count pr-str) (first rows))
                  (rest rows))
          spacers (map #(apply str (repeat % "-")) widths)
          fmts (if (= 2 (count widths))
                 [(str "%" (first widths) "s") (str "%-" (second widths) "s")]
                 (map #(str "%" % "s") widths))
          fmt-row (fn [leader divider trailer row]
                    (str leader
                         (apply str (interpose divider
                                               (for [[col fmt] (map vector row fmts)]
                                                 (format fmt (str col)))))
                         trailer))]
      ;; (.append sb "\n")
      (when header
        (.append sb (fmt-row "  " "   " "  " (map pr-str (first rows))))
        (.append sb "\n")
        (.append sb (fmt-row "--" "- -" "--" spacers))
        (.append sb "\n"))
      (doseq [row (if header
                    (next rows)
                    rows)]
        (.append sb (fmt-row "  " "   " "  " (map pr-str row)))
        (.append sb "\n"))
      (str sb))))

(defn with-left-gutter
  [string gutter]
  (reduce
   str
   ""
   (interleave (repeat gutter) (str/split-lines string) (repeat "\n"))))

(def default-line-comment ";; ")
(def default-display-width 80)

(defn- narrow-line
  [line opts]
  (let [{:keys [display-width line-comment]
         :or {display-width default-display-width
              line-comment default-line-comment}} opts
        prefix-width (count line-comment)
        target (- display-width prefix-width)
        words (str/split line #" ")
        {:keys [lines line]} (reduce
                              (fn [{:keys [line line-count lines]} word]
                                (let [word-count (count word)]
                                  (cond
                                    (> (+ line-count word-count) target)
                                    {:lines (conj lines (pop #_the-last-space line))
                                     :line [word " "]
                                     :line-count (inc word-count)}

                                    :else
                                    {:lines lines
                                     :line (conj line word " ")
                                     :line-count (+ line-count word-count 1)})))
                              {:lines []
                               :line-count 0
                               :line []}
                              words)]
    (->> (conj lines (pop #_the-last-space line))
         (map (partial str/join ""))
         (map (partial str line-comment))
         (str/join "\n"))))

(defn format-for-repl
  ([string] (format-for-repl string {}))
  ([string opts]
   (let [{:keys [line-comment]
          :or {line-comment default-line-comment}} opts
         lines (str/split-lines string)
         in-clj-block? (volatile! false)]
     (reduce
      (fn [s line]
        (cond
           ;; NOTE: Nested ``` explicitly unsupported and will break.
          (and @in-clj-block? (= line "```"))
          (do
            (vreset! in-clj-block? false)
            (str s line-comment line "\n"))

          @in-clj-block?
          (str s line "\n")

          (= line "```clj")
          (do
            (vreset! in-clj-block? true)
            (str s line-comment line "\n"))

          :else
          (str s (narrow-line line opts) "\n")))
      ""
      lines))))
