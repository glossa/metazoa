(ns glossa.metazoa.optional-deps)

;; Pattern adapted from dakrone/clj-http

(def cljfmt-enabled?
  (try
    (require 'cljfmt.core)
    true
    (catch Exception _ false)))

(def ^::recommendation recommended-cljfmt-artifacts
  '#:cljfmt{cljfmt #:mvn{:version "0.8.0"}})

(def datascript-enabled?
  (try
    (require 'datascript.core)
    true
    (catch Exception _ false)))

(def ^::recommendation recommended-datascript-artifacts
  '#:datascript{datascript #:mvn{:version "1.2.8"}})

(def malli-enabled?
  (try
    (require 'malli.core)
    true
    (catch Exception _ false)))

(def ^::recommendation recommended-malli-artifacts
  '#:metosin{malli #:mvn{:version "0.6.1"}})

(def lucene-enabled?
  (try
    (import 'org.apache.lucene.document.Document)
    true
    (catch Exception _ false)))

(def ^::recommendation recommended-lucene-artifacts
  '#:org.apache.lucene{lucene-core {:mvn/version "8.9.0"}
                       lucene-queryparser {:mvn/version "8.9.0"}})

(def recommendations
  (into []
        (comp
         (filter (comp ::recommendation meta))
         (map deref))
        (vals (ns-interns *ns*))))
