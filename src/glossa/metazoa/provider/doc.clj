(ns glossa.metazoa.provider.doc
  (:require
   [clojure.string :as str]
   [glossa.metazoa.api :as api]
   [glossa.metazoa.fmt :as fmt]
   [glossa.metazoa.provider.example :as provider.example]
   [glossa.metazoa.provider.fn-table :as provider.fn-table]
   [glossa.metazoa.util :as util]
   [glossa.weave :as weave]
   [glossa.weave.api :as weave.api]
   [glossa.weave.output.markdown :as wmd]))

(def weave-ir-mapping
  ;; NOTE: This map is sorted by value.
  {:pre-clj  ::literal-clojure-paragraph
   :code-clj ::literal-clojure
   :glossa.metazoa/example ::meta-example
   :glossa.metazoa/fn-table ::meta-fn-table})

(defmethod weave.api/parse :pre-clj
  [node]
  (weave.api/parse-shallow node))

(defmethod weave.api/parse :code-clj
  [node]
  (weave.api/parse-shallow node))

(defmethod weave.api/parse :glossa.metazoa/example
  [node]
  (let [node (if (= 2 (count node))
               [(first node) {} (second node)]
               node)
        return (weave.api/parse-shallow node)]
    return))

(defmethod weave.api/parse :glossa.metazoa/fn-table
  [node]
  (let [node (if (= 2 (count node))
               [(first node) {} (second node)]
               node)
        return (weave.api/parse-shallow node)]
    return))

(defmethod weave.api/emit [:glossa.metazoa/example wmd/output-format]
  [env instructions node]
  (let [node (update node :content (partial mapv provider.example/render))
        return (weave.api/emit-default env instructions node)]
    return))

(defmethod weave.api/emit [:glossa.metazoa/fn-table wmd/output-format]
  [env instructions node]
  (let [node (update node :content (partial mapv provider.fn-table/render))
        return (weave.api/emit-default env instructions node)]
    return))

(defmethod weave.api/render [::literal-clojure wmd/output-format]
  [env stack _]
  (let [[stack [{:keys [display]} text]] (weave.api/pop2 stack)]
    (weave.api/render-default env stack (str "`" (or display text) "`"))))

(defmethod weave.api/render [::literal-clojure-paragraph wmd/output-format]
  [env stack _]
  (let [[stack [{:keys [display]} text]] (weave.api/pop2 stack)
        rendered (or display text)
        rendered (if (str/ends-with? rendered "\n")
                   rendered
                   (str rendered "\n"))]
    (weave.api/render-default env stack (str "```clj\n" rendered "```\n\n"))))

(defmethod weave.api/render [::meta-example wmd/output-format]
  [env stack _]
  (let [[stack [_ rendered-example]] (weave.api/pop2 stack)
        rendered (if (str/ends-with? rendered-example "\n")
                   rendered-example
                   (str rendered-example "\n"))]
    (weave.api/render-default env stack (str "```clj\n" rendered "```\n\n"))))

(defmethod weave.api/render [::meta-fn-table wmd/output-format]
  [env stack _]
  (let [[stack [_ rendered-fn-table]] (weave.api/pop2 stack)
        rendered (if (str/ends-with? rendered-fn-table "\n")
                   rendered-fn-table
                   (str rendered-fn-table "\n"))
        rendered (fmt/with-left-gutter rendered "    ")]
    (weave.api/render-default env stack (str rendered "\n\n"))))

(defn- weave-doc
  [doc]
  (weave/weave
   (weave.api/env {:ir-mapping (merge weave.api/ir-mapping weave-ir-mapping)})
   doc))

(defn render
  [doc]
  (weave-doc doc))

(defn view
  [doc-string opts]
  (println (fmt/format-for-repl doc-string opts)))

(defn- node?
  [x]
  (and (map? x)
       (every? #{:tag :attrs :content} (keys x))))

(defn- check-methods []
  (dissoc (methods api/check-metadata) :glossa.metazoa/doc))

(defn- check-tree
  ([node] (check-tree  {:path [] :checks []} node))
  ([{:keys [path checks] :as all-check-results} {:keys [tag attrs content]}]
   (let [path (conj path tag)
         these-results (if (contains? (check-methods) tag)
                         (let [check-type tag
                               check-values content
                               check-metas (mapv #(merge {:glossa.metazoa/metadata-provider check-type
                                                          check-type %}
                                                         attrs)
                                                 check-values)

                               check-results (into []
                                                   (comp (mapcat #(util/ensure-sequential (api/check-metadata (with-meta [] %) check-type)))
                                                         (map #(assoc % :location path)))
                                                   check-metas)]
                           {:path path
                            :checks (concat checks check-results)})
                         (assoc all-check-results :path path))
         children-results (reduce
                           (fn [acc [idx child]]
                             (if (node? child)
                               (check-tree (update acc :path conj idx) child)
                               acc))
                           these-results
                           (map-indexed #(vector %1 %2) content))]
     (assoc children-results :path (vec (butlast path))))))

(defn check
  [weave-doc]
  (:checks (check-tree (weave/parse weave-doc))))

(defn index-for-search
  [doc]
  {:lucene
   {:field :text-field
    :stored? false
    :value (weave-doc doc)}})
