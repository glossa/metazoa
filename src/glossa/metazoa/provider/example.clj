(ns glossa.metazoa.provider.example
  (:require
   [clojure.pprint :as pprint]
   [clojure.string :as str]
   [glossa.metazoa.fmt :as fmt]
   [glossa.metazoa.optional-deps :as deps]
   [glossa.metazoa.util :refer [eval-cleanroom evoke] :as util]))

(defn- render-result
  [{:keys [format-result pretty-result]
    :or {format-result true pretty-result true}} result]
  (let [pretty-result (when pretty-result
                        (if (fn? pretty-result)
                          pretty-result
                          #(with-out-str (pprint/pprint %))))
        format-result (when format-result
                        (if (fn? format-result)
                          format-result
                          (when deps/cljfmt-enabled?
                            (requiring-resolve 'cljfmt.core/reformat-string))))]
    (cond
      (and pretty-result format-result)
      (format-result (pretty-result result))

      format-result
      (format-result (pr-str result))

      pretty-result
      (pretty-result result)

      :else (pr-str result))))

(defn- render-code-form
  [{:keys [pretty format]} code]
  (let [pretty-code (when pretty
                      (if (fn? pretty)
                        pretty
                        #(with-out-str (pprint/pprint %))))
        format-code (when format
                      (if (fn? format)
                        format
                        (when deps/cljfmt-enabled?
                          (requiring-resolve 'cljfmt.core/reformat-string))))]
    (cond
      (and pretty-code format-code)
      (format-code (pretty-code code))

      format-code
      (format-code (pr-str code))

      pretty-code
      (pretty-code code)

      :else (pr-str code))))

(defn- render-code
  [{:keys [code format pretty ns] :as exmpl}]
  (let [format (if format format (not (string? code)))
        pretty (if pretty pretty (not (string? code)))
        exmpl (assoc exmpl :format format :pretty pretty)]
    (if (string? code)
      (cond
        pretty ; handle pretty and possibly format
        (render-code-form exmpl (evoke ns `(read-string ~code)))

        format ; just format, no pretty
        (let [f (if (fn? format)
                  format
                  (when deps/cljfmt-enabled?
                    (requiring-resolve 'cljfmt.core/reformat-string)))]
          (f code))

        :else code)
      (render-code-form exmpl code))))

(def schema
  (into [:map]
        (sort
         (concat
          (rest util/schema-executable)
          [[:format {:optional true} [:or boolean? map?]]
           [:format-result {:optional true} [:or boolean? map?]]
           [:label {:optional true} string?]
           [:ns util/schema-ns]
           [:pretty {:optional true} [:or boolean? fn?]]
           [:pretty-result {:optional true} [:or boolean? fn?]]]))))

(def explainer
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/explainer) schema)))

(def validator
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/validator) schema)))

(defn bad-example
  [example]
  (let [explanation (explainer example)]
    {:error :invalid-example
          ;; :error-data explanation
     :error-explanation (util/humanize-explanation explanation)
     :example example}))

(defn render
  [example]
  (->> (util/ensure-sequential example)
       (mapv
        (fn [{:keys [code eval? label ns]
              :or {eval? true
                   ns *ns*}
              :as exmpl}]
          (if (and validator
                   (not (validator exmpl)))
            (throw (ex-info "Malformed example." (bad-example exmpl)))
            (if eval?
              (let [{:keys [result out err]} (eval-cleanroom ns code)
                    out-prefixed (when (seq out) ; allow blanks
                                   (reduce #(str %1 ";; [out] " %2 "\n")
                                           ""
                                           (str/split-lines out)))
                    err-prefixed (when (seq err) ; allow blanks
                                   (reduce #(str %1 ";; [err] " %2 "\n")
                                           ""
                                           (str/split-lines err)))
                    rendered-result (render-result exmpl result)
                    full-output (str (when label (fmt/format-for-repl label))
                                     (str/trimr (render-code exmpl))
                                     "\n"
                                     out-prefixed
                                     err-prefixed
                                     (reduce str "" (interleave (repeat "#_=> ") (str/split-lines rendered-result) (repeat "\n"))))]
                full-output)
              (str (when label (fmt/format-for-repl label))
                   (str/trimr (render-code exmpl))
                   "\n")))))
       (str/join "\n")))

(defn check
  [example]
  (mapv
   (fn [{:keys [code eval? expected satisfies expected-out expected-err ns]
         :or {eval? true
              ns *ns*}
         :as exmpl}]
     (if (and validator
              (not (validator exmpl)))
       (throw (ex-info "Malformed example." (bad-example exmpl)))
       (if eval?
         (let [pred (when satisfies
                      (if (fn? satisfies)
                        satisfies
                        (:result (eval-cleanroom ns satisfies))))]
           (try
             (let [{:keys [result out err throwable]} (eval-cleanroom ns code)

                   base-check (cond-> {:code code}
                                (contains? exmpl :expected) (assoc :expected expected)
                                out (assoc :actual-out out)
                                expected-out (assoc :expected-out expected-out)
                                err (assoc :actual-err err)
                                expected-err (assoc :expected-err expected-err)
                                throwable (assoc :throwable throwable))]
               (merge
                base-check
                (cond
                  throwable
                  nil

                  pred
                  {:actual result
                   :expected pred
                   :satisfies satisfies}

                  expected
                  {:actual result
                   :expected expected}

                  :else
                  {:actual result})))
             (catch Throwable t
               (cond-> {:throwable t}
                 expected (assoc :expected expected)
                 pred (assoc :satsifies satisfies :expected pred)))))
         {:actual :eval-skipped})))
   (util/ensure-sequential example)))
