(ns glossa.metazoa.provider.fn-table
  (:require
   [glossa.metazoa.fmt :as fmt]
   [glossa.metazoa.optional-deps :as deps]
   [glossa.metazoa.util :as util]))

(defn- unwrap-table-arg
  [arg]
  (if (and (vector? arg)
           (= 1 (count arg)))
    (first arg)
    arg))

(def schema-data
  '[:map
    [:function ifn?]
    [:label {:optional true} any?]
    [:left-hand-args sequential?]
    [:right-hand-args sequential?]])

(def schema (eval schema-data))

(def explainer
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/explainer) schema)))

(def validator
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/validator) schema)))

(defn bad-fn-table
  [fn-table-spec]
  (let [explanation (explainer fn-table-spec)]
    {:error :invalid-fn-table
     :error-explanation (util/humanize-explanation explanation)
     :fn-table fn-table-spec}))

(def default-label '_)

(defn render
  [fn-table-spec]
  (when (and validator
             (not (validator fn-table-spec)))
    (throw (ex-info "Malformed fn-table."
                    (bad-fn-table fn-table-spec))))
  (let [{:keys [left-hand-args right-hand-args label function]} fn-table-spec
        left-size (count left-hand-args)
        right-size (count right-hand-args)
        _ (when-not (= left-size right-size)
            (throw (ex-info "The :left-hand-args and :right-hand-args of a :glossa/fn-table must have the same number of items."
                            {:left-hand-args left-hand-args
                             :left-count left-size
                             :right-hand-args right-hand-args
                             :right-count right-size})))
        left-hand-args (if (some (complement vector?) left-hand-args)
                         (mapv vector left-hand-args)
                         left-hand-args)
        right-hand-args (if (some (complement vector?) right-hand-args)
                          (mapv vector right-hand-args)
                          right-hand-args)
        results (for [l left-hand-args
                      r right-hand-args]
                  (apply function (concat l r)))]
    (fmt/format-table
     {:header (into [(or label (:name (meta function)) default-label)]
                    (map unwrap-table-arg)
                    right-hand-args)
      :rows (mapv (fn [left-hand-arg results]
                    (into [(unwrap-table-arg left-hand-arg)] results))
                  left-hand-args
                  (partition right-size results))})))

(defn check
  [fn-table-spec]
  (when (and validator
             (not (validator fn-table-spec)))
    (throw (ex-info "Malformed fn-table."
                    (bad-fn-table fn-table-spec))))
  ;; NOTE: The table is built by function application and is view-only.
  ;;       The test suite of this code base asserts things about the
  ;;       format of the rendered output.
  ;;
  ;;       Implement meta/check-metadata for :glossa.metazoa/fn-table
  ;;       if you wish to make further assertions about your fn-tables.
  (try
    {:actual (render fn-table-spec)}
    (catch Throwable t
      {:throwable t
       :fn-table fn-table-spec})))
