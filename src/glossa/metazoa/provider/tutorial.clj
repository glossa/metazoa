(ns glossa.metazoa.provider.tutorial
  (:require
   [clojure.string :as str]
   [glossa.metazoa.fmt :as fmt]
   [glossa.metazoa.optional-deps :as deps]
   [glossa.metazoa.provider.doc :as doc]
   [glossa.metazoa.provider.example :as example]
   [glossa.metazoa.util :refer [evoke logln] :as util])
  (:import
   (clojure.lang PersistentVector)))

(set! *warn-on-reflection* true)

(def ^:private schema-step
  [:tuple
   keyword?
   [:map
    [:content vector?]
    [:after {:optional true} util/schema-executable]
    [:before {:optional true} util/schema-executable]
    [:clean {:optional true} util/schema-executable]]])

(def schema
  [:map
   [:title {:optional true} string?]
   [:author {:optional true} string?]
   [:ns util/schema-ns]
   [:steps
    [:and
     [:sequential schema-step]
     [:fn seq]]]])

(def explainer
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/explainer) schema)))

(def validator
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/validator) schema)))

(def ^:private -session (atom nil))

(def ^:dynamic *session* -session)

(def ^:private player-code
  (str/join "\n"
            ["               (|toc|)"
             "               (|refresh|)"
             "  (|<<|) (|<|) (|exit|) (|>|) (|>>|)"]))

(def ^:private instructions
  [:div
   [:p "Tutorials consist of a sequence of steps. The Metazoa tutorial player shows you the current step and provides functions for navigating the tutorial."]
   [:p "All tutorial functions start with the pipe character `|` for easier auto-complete and to avoid clashes with functions in your own tutorial namespaces."]
   [:p "Evaluate `(|help|)` at any time to print the player controls shown below. Their are equivalent `|first|`, `|previous|`, `|next|`, and `|last|` functions for the symbol-heavy names:"]])

(def ^:private warn-no-tutorial-msg ";; [metazoa] No tutorial in progress.")

(defn- warn-no-tutorial []
  (binding [*out* *err*]
    (logln warn-no-tutorial-msg)))

(defn- render-running-title
  [{:keys [title steps]} step-idx]
  (str "~~~~~ " (or title "Tutorial") " (" (inc step-idx) "/" (count steps) ") ~~~~~"))

(defn- render-step
  [tutorial step-idx step-content]
  (fmt/format-for-repl (str (render-running-title tutorial step-idx)
                            "\n\n"
                            (doc/render step-content))))

(defn- init-session
  [{:keys [steps] :as tutorial} original-ns]
  {:tutorial (assoc tutorial :step-keys (mapv first steps))
   :step (ffirst steps)
   :history []
   :original-ns original-ns})

(comment
  ;; Use this to populate the imported vars below.
  (->> (into []
             (comp
              (filter (fn [[sym _var]] (str/starts-with? (name sym) "|")))
              (map (fn [[sym _var]] sym)))
             (ns-publics (the-ns 'glossa.metazoa.provider.tutorial)))
       sort
       vec))

(defn- prepare-tutorial-ns
  [ns-obj]
  (in-ns (if (instance? clojure.lang.Namespace ns-obj)
           (ns-name ns-obj)
           ns-obj))
  (require '[glossa.metazoa.provider.tutorial
             :refer
             [|<<|
              |<|
              |>>|
              |>|
              |?|
              |exit|
              |first|
              |goto|
              |help|
              |last|
              |next|
              |previous|
              |quit|
              |refresh|
              |toc|]]
           '[clojure.repl :refer [apropos doc source]]))

(declare view-step)
(defn- start-tutorial!
  [{:keys [ns] :as tutorial} original-ns]
  (reset! *session* (init-session tutorial original-ns))
  (prepare-tutorial-ns ns)
  (view-step))

(defn- validate-tutorial
  [tut]
  (if validator
    (if (validator tut)
      tut
      (throw (ex-info "Malformed tutorial."
                      {:error-explanation (util/humanize-explanation (explainer tut))})))
    (do
      (binding [*out* *err*]
        (println "Skipping validation because Malli is not on the classpath..."))
      tut)))

;;
;; Tutorial Session Controls
;;

(defn- return-player []
  '[(|<|) (|toc|) (|>|)])

(defn view-step []
  (if-let [sess @*session*]
    (let [{:keys [history previous-after step tutorial]} sess
          {:keys [ns steps]} tutorial
          step-idx (.indexOf ^PersistentVector (:step-keys tutorial) step)
          [_step-kw {:keys [after before content]}] (nth steps step-idx)
          _ (when-not (= step (peek history))
              (swap! *session* update :history conj step))]
      (when previous-after (evoke ns (:code previous-after)))
      (when before (evoke ns (:code before)))
      (when after (swap! *session* assoc :previous-after after))
      (when-not (zero? step-idx)
        (logln))
      (logln (render-step tutorial step-idx content))
      (return-player))
    (warn-no-tutorial)))

(defn |toc| []
  (if-let [sess @*session*]
    (let [{:keys [steps]} (:tutorial sess)]
      (mapv (comp (partial list '|goto|) first) steps))
    (warn-no-tutorial)))

(defn |goto| [step-kw]
  (if @*session*
    (do
      (swap! *session* assoc :step step-kw)
      (view-step))
    (warn-no-tutorial)))

(comment

  (defn |history| []
    (if-let [sess @*session*]
      (let [{:keys [history]} sess
            gotos (mapv (fn [step-key] (list '|goto| step-key)) history)]
        (logln ";; [metazoa] Eval one to jump back:")
        (doseq [goto gotos]
          (prn goto))
        gotos)
      (warn-no-tutorial)))

  (defn |back|
    "History-based"
    [])

  (defn |forward|
    "History-based"
    []))

(defn- clean-step
  [ns [step-kw step-definition]]
  (when-let [clean (:clean step-definition)]
    (try
      (evoke ns clean)
      (catch Exception e
        (binding [*out* *err*]
          (println (str "Failed to cleanup step " step-kw " with message: " (.getMessage e))))))))

(defn |refresh| []
  (if-let [sess @*session*]
    (let [{:keys [step tutorial]} sess
          {:keys [ns steps]} tutorial
          step-idx (.indexOf ^PersistentVector (:step-keys tutorial) step)
          [step-kw step-definition] (nth steps step-idx)]
      (clean-step ns [step-kw step-definition])
      (swap! *session* dissoc :previous-after)
      (view-step))
    (warn-no-tutorial)))

(defn- clean-steps
  [ns steps]
  (dorun (map (partial clean-step ns) steps)))

(defn |exit| []
  (if-let [sess @*session*]
    (let [{:keys [original-ns tutorial]} sess
          {:keys [ns steps]} tutorial]
      (clean-steps ns steps)
      (reset! *session* nil)
      (in-ns (ns-name original-ns)))
    (warn-no-tutorial)))

(def |quit| |exit|)

(defn |<<|
  "Based on tutorial steps."
  []
  (if-let [{:keys [tutorial]} @*session*]
    (do
      (swap! *session* assoc :step (ffirst (:steps tutorial)))
      (view-step))
    (warn-no-tutorial)))
(def |first| |<<|)

(defn |<|
  "Based on tutorial steps."
  []
  (if-let [sess @*session*]
    (let [{:keys [step tutorial]} sess
          {:keys [^PersistentVector step-keys steps]} tutorial
          step-idx (.indexOf step-keys step)
          next-idx (dec step-idx)]
      (if (neg? next-idx)
        (logln ";; [metazoa] You're at the start of the tutorial.")
        (let [[next-step] (nth steps next-idx)]
          (swap! *session* assoc :step next-step)
          (view-step))))
    (warn-no-tutorial)))

(def |previous| |<|)

;; FEATURE: Allow configuring validation for going to the next tutorial step, so tutorial authors can ensure the learner has done the right thing and/or provide a 'give me the answer' after initial trying. Extend check functionality to include this.
(defn |>|
  "Based on tutorial steps"
  []
  (if-let [sess @*session*]
    (let [{:keys [step tutorial]} sess
          {:keys [^PersistentVector step-keys steps]} tutorial
          step-idx (.indexOf step-keys step)
          next-idx (inc step-idx)]
      (if (= next-idx (count step-keys))
        (logln ";; [metazoa] You're at the end of the tutorial.")
        (let [[next-step] (nth steps next-idx)]
          (swap! *session* assoc :step next-step)
          (view-step))))
    (warn-no-tutorial)))

(def ^{:glossa.metazoa/alias #'|>|}
  |next| |>|)

(defn |>>|
  "Based on tutorial steps."
  []
  (if-let [{:keys [tutorial]} @*session*]
    (do
      (swap! *session* assoc :step ((comp first last) (:steps tutorial)))
      (view-step))
    (warn-no-tutorial)))
(def |last| |>>|)

(defn |help| []
  (logln (fmt/format-for-repl (doc/render instructions)))
  (logln player-code))
(def |?| |help|)

(defn view
  [tutorial]
  (-> (validate-tutorial tutorial)
      (start-tutorial! *ns*)))

(defn- check-tutorial!
  [tutorial original-ns]
  (let [{:keys [ns steps]} tutorial]
    (reset! *session* (init-session tutorial original-ns))
    (prepare-tutorial-ns ns)
    (reduce
     (fn [checks [_step-kw {:keys [before after content] :as _step-definition}]]
       (let [before-check (when before
                            (example/check (assoc before :ns ns)))
             after-check (when after
                           (example/check (assoc after :ns ns)))
             content-check (doc/check content)]
         (into checks
               (comp (remove nil?)
                     (mapcat identity))
               [before-check after-check content-check])))
     []
     steps)))

(defn check
  [tutorial]
  (let [tutorial (validate-tutorial tutorial)]
    (binding [*session* (atom nil)]
      (check-tutorial! tutorial *ns*))))

(defn tutorial
  [tut]
  (validate-tutorial tut))
