(ns glossa.metazoa.query
  (:require
   [glossa.metazoa.optional-deps :as deps]))

(def ^:private ds 'glossa.metazoa.query.datascript)

(when deps/datascript-enabled?
  (require ds))

(defn- throw-datascript-required
  []
  (throw (ex-info "You must add Datascript to your classpath to use `meta/query`."
                  {:problem :unsatisfied-dependencies
                   :dependencies deps/recommended-datascript-artifacts})))

;; Convenience methods for accessing and indexing the default DataScript database.
;; Use the functions in glossa.metazoa.query.datascript directly if you need
;; to pass in a different DataScript connection/database value.

;; NOTE: Named `index` for consistency with glossa.metazoa.search
(defn index
  ([index-spec]
   (if deps/datascript-enabled?
     ((requiring-resolve 'glossa.metazoa.query.datascript/populate-database) index-spec)
     (throw-datascript-required))))

(defn reset
  ([]
   (if deps/datascript-enabled?
     ((requiring-resolve 'glossa.metazoa.query.datascript/reset))
     (throw-datascript-required)))
  ([index-spec]
   (if deps/datascript-enabled?
     ((requiring-resolve 'glossa.metazoa.query.datascript/reset) index-spec)
     (throw-datascript-required))))

(defn db
  []
  (if deps/datascript-enabled?
    ((requiring-resolve 'glossa.metazoa.query.datascript/db))
    (throw-datascript-required)))

(defn q
  [datalog-query & args]
  (if deps/datascript-enabled?
    (let [q-fn (requiring-resolve 'glossa.metazoa.query.datascript/q)
          db (db)]
      (apply q-fn datalog-query db args))
    (throw-datascript-required)))
