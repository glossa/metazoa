(ns glossa.metazoa.query.datascript
  (:require
   [datascript.core :as d]
   [glossa.metazoa.api :as api]
   [glossa.metazoa.util :refer [logln] :as util])
  (:import
   (java.util.concurrent.atomic AtomicLong)))

(set! *warn-on-reflection* true)

(def imeta-schema
  {:imeta/symbol     {:db/doc "Symbol of the IMeta (namespace or var)."}
   :imeta/this       {:db/doc "The IMeta object itself (usually a namespace or var)."}
   :imeta/type       {:db/doc "Value of (type imeta), e.g., clojure.lang.Namespace."}
   :imeta/value      {:db/doc "If the IMeta is a Var, this is the underlying value held by the var."}
   :imeta/value-type {:db/doc "If the IMeta is a Var, this is the type of the underlying value held by the var."}})

(defonce ^:private temp-id
  (AtomicLong. 0))

(defn- next-temp-id []
  (.decrementAndGet ^AtomicLong temp-id))

(def ^:private -conn (atom nil))

(defn- imetas->tx-data
  [imetas]
  (into []
        (comp (map (juxt identity meta))
              (remove (comp nil? second))
              (map (fn [[imeta mm]]
                     (let [id (api/db-id imeta)
                           entity {:db/id (next-temp-id)
                                   :imeta/this imeta
                                   :imeta/symbol id
                                   :imeta/type (type imeta)}
                           entity (if (var? imeta)
                                    (let [var-value (deref imeta)]
                                      (merge entity
                                             (when-not (nil? var-value)
                                               {:imeta/value var-value
                                                :imeta/value-type (type var-value)})))
                                    entity)]
                       (reduce-kv
                        (fn [entity mk mv]
                          (if (and (keyword? mk)
                                   (not (nil? mv)))
                            (assoc entity mk mv)
                            entity))
                        entity
                        mm)))))
        imetas))

(def ^:private clj-kondo-analysis-schema
  {;; :namespace-definitions ;;

   :namespace-definition/added {}
   :namespace-definition/author {}
   :namespace-definition/col {}
   :namespace-definition/deprecated {}
   :namespace-definition/doc {}
   :namespace-definition/filename {}
   :namespace-definition/lang {}
   :namespace-definition/name {}
   :namespace-definition/no-doc {}
   :namespace-definition/row {}

   ;; :namespace-usages ;;

   :namespace-usage/alias {}
   :namespace-usage/col {}
   :namespace-usage/filename {}
   :namespace-usage/from {}
   :namespace-usage/lang {}
   :namespace-usage/ref-from-ns {:db/doc "Reference to the entity for the namespace specified by :namespace-usage/from, which is the 'source' namespace (the one relying on another in this usage site)."}
   :namespace-usage/ref-to-ns {:db/doc "Reference to the entity for the namespace specified by :namespace-usage/to, which is the 'target' namespace (the one being relied upon in this usage site)."}
   :namespace-usage/row {}
   :namespace-usage/to {}

   ;; :var-definitions ;;

   :var-definition/added {}
   :var-definition/col {}
   :var-definition/defined-by {:db/doc "Fully-qualified symbol of macro used to define this var if not def or defn."}
   :var-definition/deprecated {}
   :var-definition/doc {}
   :var-definition/filename {}
   :var-definition/fixed-arities {}
   :var-definition/macro {}
   :var-definition/name {}
   :var-definition/ns {}
   :var-definition/private {}
   :var-definition/row {}
   :var-definition/test {}
   :var-definition/varargs-min-arity {}

   ;; :var-usages ;;

   :var-usage/arity {:db/doc "The arity of the invocation at this call site."}
   :var-usage/col {}
   :var-usage/deprecated {}
   :var-usage/filename {}
   :var-usage/fixed-arities {:db/doc "Set of fixed arities the called function supports."}
   :var-usage/from {}
   :var-usage/from-var {:db/doc "Symbol of the var name of the containing var definition in which this call-site is located."}
   :var-usage/lang {}
   :var-usage/macro {}
   :var-usage/name {:db/doc "Symbol of the var name for the function called at this call-site. See :var-usage/to for the namespace."}
   :var-usage/private {}
   :var-usage/ref-from-ns {:db/doc "Reference to the entity for the namespace specified by :var-usage/from, which is the 'source' namespace (the one relying on another in this usage site)."}
   :var-usage/ref-from-var {:db/doc "Reference to the entity for the var specified by :var-usage/from (the namespace) and :var-usage/from-var (the var name), which is the 'source' var (the one relying on another in this usage site)."}
   :var-usage/ref-to-ns {:db/doc "Reference to the entity for the namespace specified by :var-usage/to, which is the 'target' namespace (the one being relied upon in this usage site)."}
   :var-usage/ref-to-var {:db/doc "Reference to the entity for the var specified by :var-usage/to (the namespace) and :var-usage/name (the var name), which is the 'target' var (the one being relied upon in this usage site)."}
   :var-usage/row {}
   :var-usage/to {:db/doc "Symbol of the namespace in which the function called at this call-site is defined. See :var-usage/name for the var name."}
   :var-usage/varargs-min-arity {}})

(def ^:private entities
  "Keys are the top-level entires of clj-kondo's analysis map;
  values are the 'entity names' as reified in datom attributes."
  {:namespace-definitions :namespace-definition
   :namespace-usages      :namespace-usage
   :var-definitions       :var-definition
   :var-usages            :var-usage})

(def ^:private base-schema
  "Base DataScript schema. Can be extended by implementing
  `glossa.metazao.api/query-schema`"
  (merge
   imeta-schema
   clj-kondo-analysis-schema))

(defn symbol-nil
  [ns-sym name-sym]
  (when (and ns-sym name-sym)
    (symbol (name ns-sym) (name name-sym))))

(declare q)
(defn- analysis->tx-data
  "Expects to be run after the initial `imetas->tx-data`."
  [db analysis]
  (reduce-kv
   (fn [tx-data analysis-key analysis-data]
     (if-let [entity (get entities analysis-key)]
       (concat tx-data
               (sequence
                (comp
                 (map
                  (fn [analysis-map]
                    (let [db-id (case entity
                                  :namespace-definition
                                  (q '[:find ?e .
                                       :in $ ?the-ns
                                       :where
                                       [?e :imeta/this ?the-ns]]
                                     db (util/requiring-resolve+ (:name analysis-map)))

                                  :var-definition
                                  (q '[:find ?e .
                                       :in $ ?the-var
                                       :where
                                       [?e :imeta/this ?the-var]]
                                     db (util/requiring-resolve+ (symbol-nil (:ns analysis-map)
                                                                             (:name analysis-map))))

                                  #_else nil)
                          tx-data-map (reduce-kv
                                       (fn [tx-data-map k v]
                                         (if (nil? v)
                                           tx-data-map
                                           (case entity
                                             :namespace-usage
                                             (case k
                                               :from
                                               (let [from (util/requiring-resolve+ (:from analysis-map))
                                                     eid (when from
                                                           (q '[:find ?e .
                                                                :in $ ?the-ns
                                                                :where
                                                                [?e :imeta/this ?the-ns]]
                                                              db from))]
                                                 (cond-> (assoc tx-data-map :namespace-usage/from v)
                                                   eid (assoc :namespace-usage/ref-from-ns eid)))

                                               :to
                                               (let [to (util/requiring-resolve+ (:to analysis-map))
                                                     eid (when to
                                                           (q '[:find ?e .
                                                                :in $ ?the-ns
                                                                :where
                                                                [?e :imeta/this ?the-ns]]
                                                              db to))]
                                                 (cond-> (assoc tx-data-map :namespace-usage/to v)
                                                   eid (assoc :namespace-usage/ref-to-ns eid)))

                                               #_else (assoc tx-data-map (keyword (name entity) (name k)) v))

                                             :var-usage
                                             (case k
                                               :from
                                               (let [from (util/requiring-resolve+ (:from analysis-map))
                                                     eid (when from
                                                           (q '[:find ?e .
                                                                :in $ ?the-ns
                                                                :where
                                                                [?e :imeta/this ?the-ns]]
                                                              db from))]
                                                 (cond-> (assoc tx-data-map :var-usage/from v)
                                                   eid (assoc :var-usage/ref-from-ns eid)))

                                               :from-var
                                               (let [from-var (util/requiring-resolve+ (symbol-nil (:from analysis-map)
                                                                                                   (:from-var analysis-map)))
                                                     eid (when from-var
                                                           (q '[:find ?e .
                                                                :in $ ?the-var
                                                                :where
                                                                [?e :imeta/this ?the-var]]
                                                              db from-var))]
                                                 (cond-> (assoc tx-data-map :var-usage/from-var v)
                                                   eid (assoc :var-usage/ref-from-var eid)))

                                               :to
                                               (let [to (util/requiring-resolve+ (:to analysis-map))
                                                     eid (when to
                                                           (q '[:find ?e .
                                                                :in $ ?the-ns
                                                                :where
                                                                [?e :imeta/this ?the-ns]]
                                                              db to))]
                                                 (cond-> (assoc tx-data-map :var-usage/to v)
                                                   eid (assoc :var-usage/ref-to-ns eid)))

                                               :name
                                               (let [nm (util/requiring-resolve+ (symbol-nil (:to analysis-map)
                                                                                             (:name analysis-map)))
                                                     eid (when nm
                                                           (q '[:find ?e .
                                                                :in $ ?the-var
                                                                :where
                                                                [?e :imeta/this ?the-var]]
                                                              db nm))]
                                                 (cond-> (assoc tx-data-map :var-usage/name v)
                                                   eid (assoc :var-usage/ref-to-var eid)))

                                               #_else (assoc tx-data-map (keyword (name entity) (name k)) v))

                                             #_else (assoc tx-data-map (keyword (name entity) (name k)) v))))
                                       (cond-> {}
                                         db-id (assoc :db/id db-id))
                                       analysis-map)]
                      (when (seq tx-data-map)
                        (if-not (:db/id tx-data-map)
                          (assoc tx-data-map :db/id (next-temp-id))
                          tx-data-map)))))
                 (remove nil?))
                analysis-data))
       tx-data))
   []
   analysis))

(defn- create-connection
  [conn-atom]
  (let [schema-methods (methods api/query-schema)
        schema (reduce-kv
                (fn [schema _ schema-fn]
                  (let [schema-additions (schema-fn #_imeta nil #_k nil)]
                    (merge schema schema-additions)))
                base-schema
                schema-methods)]
    (reset! conn-atom (d/create-conn schema))))

(defn- default-index-spec []
  (let [imetas (api/find-imetas)
        analysis (api/find-cached-analysis)]
    {:imetas imetas :analysis analysis}))

(defn populate-database
  ([index-spec]
   (populate-database -conn index-spec))
  ([connection-atom {:keys [analysis imetas] :as _index-spec}]
   (create-connection -conn)
   (d/transact! @connection-atom (imetas->tx-data imetas))
   (d/transact! @connection-atom (analysis->tx-data @@connection-atom analysis))))

(defn reset
  ([] (reset (default-index-spec)))
  ([index-spec]
   (logln "Re-transacting metadata for datalog querying...")
   (populate-database index-spec)
   :ok))

(defn db []
  (if-let [conn @-conn]
    @conn
    (do
      (logln "Transacting metadata for datalog querying...")
      (populate-database (api/find-imetas))
      @@-conn)))

(defn q
  [query & [maybe-db-1 maybe-db-2 :as args]]
  (let [args (if (and (d/db? maybe-db-1)
                      (d/db? maybe-db-2)
                      (= maybe-db-1 maybe-db-2))
               ;; glossa.metazoa.query/q adds a db unconditionally, to avoid
               ;; having further conditional resolve calls there. This removes
               ;; it if the user also provided one.
               (next args)
               args)]
    (apply d/q query args)))

(comment

  (reset (api/find-imetas))

  (count
   (q '[:find ?ns ?name ?added
        :in $ ?ns [?added ...]
        :where
        [?e :ns ?ns]
        [?e :name ?name]
        [?e :added ?added]]
      (db)
      (the-ns 'clojure.core)
      ["1.5" "1.6" "1.7"])))
