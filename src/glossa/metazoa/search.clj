(ns glossa.metazoa.search
  (:require
   [clojure.set :as set]
   [glossa.metazoa.api :as api]
   [glossa.metazoa.optional-deps :as deps]
   [glossa.metazoa.util :as util]))

(def ^:private l 'glossa.metazoa.search.lucene)

(when deps/lucene-enabled?
  (require l))

(defn- throw-lucene-required
  []
  (throw (ex-info "You must add Lucene to your classpath to use `meta/search`."
                  {:problem :unsatisfied-dependencies
                   :dependencies deps/recommended-lucene-artifacts})))

(defn index
  ([] (index (api/find-imetas)))
  ([imetas]
   (if deps/lucene-enabled?
     ((requiring-resolve 'glossa.metazoa.search.lucene/index) imetas)
     (throw-lucene-required))))

(defn reset
  ([] (reset (api/find-imetas)))
  ([imetas]
   (if deps/lucene-enabled?
     ((requiring-resolve 'glossa.metazoa.search.lucene/reset) imetas)
     (throw-lucene-required))))

(def default-num-hits 30)

(def schema-search-query
  [:or
   string?
   [:fn {:error/fn (fn [{:keys [_value]} _] "should be a clojure.lang.Named")}
    #(instance? clojure.lang.Named %)]
   [:map
    ;; NOTE: :limit is a synonym for :num-hits
    [:limit {:optional true} integer?]
    [:query string?]
    [:num-hits {:optional true} integer?]]])

(def validator-search-query
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/validator) schema-search-query)))

(def explainer-search-query
  (when deps/malli-enabled?
    ((requiring-resolve 'malli.core/explainer) schema-search-query)))

(defn search-query
  [user-query]
  (when validator-search-query
    (when-not (validator-search-query user-query)
      (let [error-data (explainer-search-query user-query)
            error-explanation (util/humanize-explanation error-data)]
        (throw (ex-info (str "Invalid search query: " error-explanation)
                        {:error-data error-data
                         :error-explanation error-explanation})))))
  (if (or (string? user-query)
          (instance? clojure.lang.Named user-query))
    {:query (util/full-name user-query)
     :num-hits default-num-hits}
    (merge {:num-hits default-num-hits}
           (set/rename-keys user-query {:limit :num-hits}))))

(defn search
  [query]
  (if deps/lucene-enabled?
    (let [search-fn (requiring-resolve 'glossa.metazoa.search.lucene/search)
          query (search-query query)]
      (search-fn query))
    (throw-lucene-required)))
