(ns glossa.metazoa.search.lucene
  (:require
   [clojure.java.io :as io]
   [glossa.metazoa.api :as api]
   [glossa.metazoa.util :refer [logln] :as util])
  (:import
   (java.io File)
   (org.apache.lucene.analysis.standard StandardAnalyzer)
   (org.apache.lucene.document Document Field$Store IntPoint StringField TextField)
   (org.apache.lucene.index DirectoryReader IndexWriter IndexWriterConfig IndexWriterConfig$OpenMode)
   (org.apache.lucene.queryparser.classic MultiFieldQueryParser)
   (org.apache.lucene.search IndexSearcher ScoreDoc)
   (org.apache.lucene.store Directory NIOFSDirectory)))

(set! *warn-on-reflection* true)

(def ^:private lucene-directory (atom nil))
(def ^:private lucene-index-reader (atom nil))

(defn- new-fs-dir
  ([]
   (new-fs-dir (str (or (System/getProperty "java.io.tmpdir")
                        "/tmp")
                    File/separator
                    "_glossa_metazoa")))
  ([path-str]
   (let [path (-> path-str
                  (io/file)
                  (.toPath))]
     (NIOFSDirectory. path))))

(def ^:dynamic *directory-ctor* new-fs-dir)

(def ^String lucene-full-name
  (partial util/full-name "_"))

(def fields-to-multi-search
  (atom #{"glossa.metazoa/doc"
          "name"
          "ns"}))

(defn add-to-multi-search
  [metadata-key]
  (swap! fields-to-multi-search conj (lucene-full-name metadata-key)))

(defn default-analyzer []
  (StandardAnalyzer.))

(def field-stored?
  {true Field$Store/YES false Field$Store/NO})

(defn- execute-index-spec
  [{:keys [lucene] :as index-spec} ^Document doc metadata-key metadata-value]
  (when lucene
    (let [{:keys [index-fn field value stored?]} lucene]
      (if index-fn
        (index-fn doc metadata-key metadata-value)
        (case field
          :text-field (.add doc (TextField.
                                 (lucene-full-name metadata-key)
                                 (or value (str metadata-value))
                                 (get field-stored? (boolean stored?) Field$Store/NO)))
          (throw (ex-info "Currently only a :text-field value is supported in :field. Supply an :index-fn instead to handle indexing your field."
                          {:error :unsupported-field
                           :unsupported-field field
                           :index-spec index-spec}))))
      (add-to-multi-search metadata-key))))

(defn doc-id-clj
  [^Document doc]
  (let [doc-type (.get doc "imeta-type")
        id (.get doc "id")]
    (case doc-type
      ("clojure.lang.Var" "clojure.lang.Namespace")
      (util/requiring-resolve+ (symbol id))

      id)))

(defn- imeta-document [imeta]
  (let [imeta-type (class imeta)
        ^String doc-id (api/doc-id imeta)
        value-type (when (var? imeta)
                     (let [var-value (deref imeta)]
                       (when-not (nil? var-value)
                         (.getName (class var-value)))))
        doc (doto (Document.)
              (.add (StringField. "id" doc-id Field$Store/YES))
              (.add (StringField. "imeta-symbol" doc-id Field$Store/YES))
              (.add (TextField. "imeta-type" (.getName imeta-type) Field$Store/YES)))
        _ (when value-type
            (.add doc (TextField. "imeta-value-type" value-type Field$Store/NO)))]
    doc))

(def failed-imetas (atom []))

(defn metadata->documents
  [imetas]
  (into []
        (comp (map (juxt identity meta))
              (remove (comp nil? second))
              (map (fn [[imeta mm]]
                     (reduce-kv
                      (fn [^Document doc mk mv]
                        (let [index-spec (try
                                           (api/index-for-search (with-meta [] mm) mk)
                                           (catch Exception _
                                             (swap! failed-imetas conj imeta)
                                             (binding [*out* *err*]
                                               (println
                                                (str "Failed to index IMeta "
                                                     imeta
                                                     " at metadata key "
                                                     mk ".\n"
                                                     "Try invoking (index @failed-imetas) from the glossa.metazoa.search.lucene namespace\nto index them again.")))
                                             nil))]
                          (cond
                            index-spec
                            (execute-index-spec index-spec doc mk mv)

                            (= mk :column)
                            (.add doc (IntPoint. "column" (int-array [mv])))

                            (= mk :line)
                            (.add doc (IntPoint. "line" (int-array [mv])))

                            (= mk :ns)
                            (.add doc (StringField. "ns" (str mv) Field$Store/NO))

                            (= mk :name)
                            (.add doc (TextField. "name" (str mv) Field$Store/NO))

                            (not (nil? mv))
                            (do
                              (.add doc (TextField. (lucene-full-name mk) (str mv) Field$Store/NO))
                              (add-to-multi-search mk))))
                        doc)
                      (imeta-document imeta)
                      mm))))
        imetas))

;; Drawn from here:
;; https://lucene.apache.org/core/8_9_0/demo/src-html/org/apache/lucene/demo/IndexFiles.html
(defn index
  ([] (index (api/find-imetas)))
  ([imetas]
   (let [analyzer (default-analyzer)
         writer-cfg (doto (IndexWriterConfig. analyzer)
                      (.setOpenMode IndexWriterConfig$OpenMode/CREATE))
         dir (or @lucene-directory
                 (reset! lucene-directory (*directory-ctor*)))]
     (try
       (with-open [w (IndexWriter. dir writer-cfg)]
         (let [docs (metadata->documents imetas)]
           (doseq [doc docs]
             (.addDocument w doc))))
       (catch Exception e
         (throw (ex-info "Failed to index metadata." {} e)))))))

(defn reset
  [imetas]
  (reset! lucene-directory nil)
  (logln "Reindexing metadata for full-text search...")
  (index imetas)
  :ok)

(defn search
  [{:keys [^int num-hits query]}]
  (let [dir (or @lucene-directory
                (do
                  (logln "Indexing metadata for full-text search...")
                  (index)
                  @lucene-directory))
        fields @fields-to-multi-search
        index-reader (or @lucene-index-reader
                         (reset! lucene-index-reader (DirectoryReader/open ^Directory dir)))
        index-searcher (IndexSearcher. ^DirectoryReader index-reader)
        query-parser (MultiFieldQueryParser. (into-array String fields) (default-analyzer))
        query (.parse query-parser query)
        top-docs (.search index-searcher query num-hits)
        score-docs (.scoreDocs top-docs)
        result (reduce
                (fn [acc ^ScoreDoc score-doc]
                  (let [doc-num (.doc score-doc)
                        doc (.doc index-searcher doc-num)
                        clj-value (doc-id-clj doc)]
                    (-> acc
                        (update :results conj (or clj-value
                                                  [(.get doc "imeta-type") (.get doc "id")]))
                        (update :docs conj doc))))
                {:results []
                 :docs []}
                score-docs)]
    (with-meta
      (:results result)
      {:docs (:docs result)
       :top-docs top-docs
       :total-hits (-> top-docs .totalHits .-value)})))

(comment
  (time (index))
  (time (search "source")))
