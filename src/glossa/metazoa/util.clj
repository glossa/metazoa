(ns glossa.metazoa.util
  (:require
   [clojure.string :as str]
   [glossa.metazoa.optional-deps :as deps])
  (:import
   (java.io StringWriter)))

(defn evoke
  "Eval or invoke."
  ([evokable]
   (if (fn? evokable)
     (evokable)
     (eval evokable)))
  ([ns evokable]
   (if (fn? evokable)
     (evokable)
     (if ns
       (binding [*ns* (if (symbol? ns)
                        (do
                          (require ns)
                          (the-ns ns))
                        ns)]
         (let [return (eval evokable)]
           return))
       (eval evokable)))))

(defn eval-cleanroom
  [ns code]
  (let [code-form (if (string? code)
                    (evoke ns `(read-string ~code))
                    code)
        out (StringWriter.)
        err (StringWriter.)
        res (binding [*out* out
                      *err* err]
              (evoke ns code-form))
        out-str (str out)
        err-str (str err)]
    {:result res
     :out out-str
     :err err-str}))

(defn log
  [x]
  (if (string? x)
    (print x)
    (pr x)))

(defn logln
  ([] (log "\n"))
  ([x]
   (log x) (log "\n")))

(defn ensure-sequential [x]
  (if (sequential? x)
    x
    [x]))

(defn ^String full-name
  ([x] (full-name "/" x))
  ([sep x]
   (cond
     (string? x) x

     (qualified-ident? x)
     (str (namespace x) sep (name x))

     (instance? clojure.lang.Named x)
     (name x)

     :else (str x))))

(defn load-namespaces
  [imeta]
  (when-let [namespace-syms (get (meta imeta) :glossa.metazoa/namespaces)]
    (doseq [ns-sym namespace-syms]
      (require ns-sym :reload))))

(defn requiring-resolve+
  [sym]
  (if (symbol? sym)
    (try
      (if (namespace sym)
        (requiring-resolve sym)
        (the-ns sym))
      (catch Exception _
        nil))
    sym))

(defn align-code-str
  ([s] (align-code-str 2 s))
  ([n s]
   (let [[line-0 line-1 :as lines] (str/split-lines s)
         num-spaces-delete (- (count (second (re-find #"^(\s+)" line-1)))
                              n)
         aligned (into [line-0]
                       (mapv #(subs % num-spaces-delete) (next lines)))]
     (str/join "\n" aligned))))

(def schema-ns
  [:or
   symbol?
   [:fn #(instance? clojure.lang.Namespace %)]])

(def schema-executable
  [:map
   [:code any?]
   [:expected {:optional true} any?]
   [:expected-err {:optional true} string?]
   [:expected-out {:optional true} string?]
   [:satisfies {:optional true} any?]])

(def humanize-explanation
  (when deps/malli-enabled?
    (requiring-resolve 'malli.error/humanize)))
