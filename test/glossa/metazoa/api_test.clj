(ns glossa.metazoa.api-test
  (:require
   [clojure.test :refer [deftest is]]
   [glossa.metazoa.api :as nut]))

(defn- exclude-clojure-core
  [ns]
  (when (not= (the-ns 'clojure.core) ns)
    ((comp vals ns-publics) ns)))

(defn- only-clojure-core
  [ns]
  (when (= (the-ns 'clojure.core) ns)
    ((comp vals ns-publics) ns)))

(def ^:private all-interns (comp vals ns-interns))

(deftest test-find-imetas
  (let [count-all-public (count (nut/find-imetas (comp vals ns-publics)))
        count-exclude-clojure-core (count (nut/find-imetas exclude-clojure-core))
        count-only-clojure-core (count (nut/find-imetas only-clojure-core))
        count-all-interns (count (nut/find-imetas all-interns))]
    (is (> count-all-public 1000))
    (is (= count-all-public
           (+ count-exclude-clojure-core count-only-clojure-core)))
    (is (> count-all-interns count-all-public))))

(deftest test-metadata-providers
  (let [providers (nut/metadata-providers)]
    (is (= #{:render :view :check :all}
           (set (keys providers))))
    (is (every? seq (vals providers)))))
