(ns glossa.metazoa.optional-deps-test
  (:require
   [clojure.edn :as edn]
   [clojure.test :refer [deftest is]]
   [glossa.metazoa.optional-deps :as nut]))

(def deps (:deps (edn/read-string (slurp "deps.edn"))))

(defn find-deps [xs]
  (into {} (filter (fn [[k _v]] (contains? deps k)) xs)))

(deftest test-recommended-artifacts
  (is (= nut/recommended-cljfmt-artifacts (find-deps nut/recommended-cljfmt-artifacts)))
  (is (= nut/recommended-datascript-artifacts (find-deps nut/recommended-datascript-artifacts)))
  (is (= nut/recommended-lucene-artifacts (find-deps nut/recommended-lucene-artifacts)))
  (is (= nut/recommended-malli-artifacts (find-deps nut/recommended-malli-artifacts))))

(deftest test-optional-deps-tested
  (is (= '[cljfmt/cljfmt
           datascript/datascript
           metosin/malli
           org.apache.lucene/lucene-core
           org.apache.lucene/lucene-queryparser]
         (sort (mapcat keys nut/recommendations)))
      "Ensure all recommended deps are tested in this namespace."))
