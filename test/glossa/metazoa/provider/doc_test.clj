(ns glossa.metazoa.provider.doc-test
  (:require
   [clojure.test :refer [deftest is]]
   [glossa.metazoa-meta :as metazoa-meta]
   [glossa.metazoa.provider.doc :as nut]))

(deftest test-render
  (let [readme-doc (metazoa-meta/readme {:version "VERSION" :sha "SHA" :tag "TAG"})
        rendered-readme (nut/render readme-doc)]
    (is (string? rendered-readme))
    (is (< 8000 (count rendered-readme)))
    (is (re-find #"view" rendered-readme))
    (is (re-find #"search" rendered-readme))
    (is (re-find #"query" rendered-readme))
    (is (re-find #"check" rendered-readme))))

(defn- doc-check?
  [check]
  (and (contains? check :actual)
       (contains? check :location)))

(deftest test-check
  (is (every? doc-check? (nut/check (metazoa-meta/readme {:version "VERSION" :sha "SHA" :tag "TAG"})))))
