(ns glossa.metazoa.provider.example-test
  (:require
   [clojure.test :refer [deftest is]]
   [glossa.metazoa.provider.example :as nut])
  (:import
   (clojure.lang ExceptionInfo)))

(def example
  {:label "Test example"
   :code '(seq [1 2 3])
   :ns *ns*})

(deftest test-example|schema
  (is (nut/validator example)
      (nut/explainer example)))

(deftest test-example|passing|quoted-form
  (let [[{:keys [actual] :as _check}] (nut/check example)]
    (is (= '(1 2 3) actual))))

(deftest test-example|passing|fn-form
  (let [[{:keys [actual] :as _check}]
        (nut/check (assoc example :code (fn [] (seq [1 2 3]))))]
    (is (= '(1 2 3) actual))))

(deftest test-example|passing|test-expected
  (let [[{:keys [expected actual] :as _check}]
        (nut/check (assoc example :expected '(1 2 3)))]
    (is (= [1 2 3] expected actual))))

(deftest test-example|passing|test-satisfies-fn-form
  (let [[{:keys [actual satisfies] :as _check}]
        (nut/check (assoc example :satisfies (comp odd? count)))]
    (is (fn? satisfies))
    (is actual)))

(deftest test-example|passing|test-satisfied-quoted-form
  (let [[{:keys [actual satisfies] :as _check}]
        (nut/check (assoc example :satisfies '(comp odd? count)))]
    ;; NOTE: Unused at this level, but made available for better test failure reporting.
    (is (list? satisfies))
    (is actual)))

(def example|broken
  {:label "Broken example"
   :code '(throw (ex-info "Error!" {::type ::test}))
   :expected ""
   :ns *ns*})

(deftest test-example|broken|check|invalid-example
  (try
    (nut/check (dissoc example|broken :ns))
    (is nil "Above example should throw an exception when checked.")
    (catch clojure.lang.ExceptionInfo e
      (let [{:keys [error error-explanation]} (ex-data e)]
        (is (= :invalid-example error))
        (is (= {:ns ["missing required key"]} error-explanation))))))

(deftest test-example|broken|render|invalid-example
  (try
    (nut/render (dissoc example|broken :ns))
    (is nil "Above example should throw an exception when rendered.")
    (catch clojure.lang.ExceptionInfo e
      (let [{:keys [error error-explanation]} (ex-data e)]
        (is (= :invalid-example error))
        (is (= {:ns ["missing required key"]} error-explanation))))))

(deftest test-example|failing|throw-exception
  (let [[{:keys [throwable expected] :as _check}]
        (nut/check example|broken)]
    (is (instance? ExceptionInfo throwable))
    (is (= ::test (::type (ex-data throwable))))
    (is (= expected ""))))

(deftest test-example|failing|mismatched-expected
  (let [[{:keys [expected actual]}]
        (nut/check (assoc example :expected '(4 5 6)))]
    (is (= [1 2 3] actual))
    (is (= [4 5 6] expected))))

(deftest test-example|failing|not-satisfies
  (let [[{:keys [actual expected satisfies]}]
        (nut/check (assoc example :satisfies '(comp even? count)))]
    (is (not (expected actual)))
    (is (= satisfies '(comp even? count)))))

(def example|multi
  [{:label "Example Alpha"
    :code '(first "a")
    :expected \a
    :ns *ns*}
   {:label "Example Beta"
    :code '(first "b")
    :satisfies 'char?
    :ns *ns*}])

(deftest test-example|multi|passing
  (is (= [{:actual \a :expected \a}
          {:actual \b :satisfies 'char? :expected char?}]
         (mapv #(select-keys % [:actual :expected :satisfies])
               (nut/check example|multi)))))
