(ns glossa.metazoa.provider.fn-table-test
  (:require
   [clojure.string :as str]
   [clojure.test :refer [deftest is]]
   [glossa.metazoa.provider.fn-table :as nut]
   [malli.core :as m]))

(def subtract-fn-table-spec
  {:function -
   :left-hand-args (range -3 4)
   :right-hand-args (range -3 4)})

(def fn-table-valid? (m/validator nut/schema))

(deftest test-check
  ;; NOTE: See explanation in implementation ns.
  (is (fn-table-valid? subtract-fn-table-spec))
  (is (nut/check subtract-fn-table-spec)))

(deftest test-fn-table-output-format
  (let [fn-table-spec subtract-fn-table-spec
        {:keys [left-hand-args right-hand-args]} fn-table-spec
        left-size (count left-hand-args)
        right-size (count right-hand-args)
        _ (when-not (= left-size right-size)
            (throw (ex-info "The :left-hand-args and :right-hand-args of a :glossa.metazoa/fn-table must have the same number of items."
                            {:left-hand-args left-hand-args
                             :left-count left-size
                             :right-hand-args right-hand-args
                             :right-count right-size})))
        rendered-fn-table (nut/render fn-table-spec)
        sep-re #"^-+\s(?:-+\s)*-+$"
        [header sep & rows :as _lines] (str/split-lines rendered-fn-table)]
    (is (str/includes? header (name nut/default-label)))
    (is (re-find sep-re sep))
    (is (= left-size (count rows)))))

(deftest test-fn-table-subtract
  (is (= (str/join "\n"
                   ["   _   -3   -2   -1    0    1    2    3  "
                    "----- ---- ---- ---- ---- ---- ---- -----"
                    "  -3    0   -1   -2   -3   -4   -5   -6  "
                    "  -2    1    0   -1   -2   -3   -4   -5  "
                    "  -1    2    1    0   -1   -2   -3   -4  "
                    "   0    3    2    1    0   -1   -2   -3  "
                    "   1    4    3    2    1    0   -1   -2  "
                    "   2    5    4    3    2    1    0   -1  "
                    "   3    6    5    4    3    2    1    0  "
                    ""])
         (nut/render subtract-fn-table-spec))))

(deftest test-fn-table-max
  (let [fn-table-spec {:function max
                       :left-hand-args  [0 1]
                       :right-hand-args [0 1]
                       :label           'OR}]
    (is (fn-table-valid? fn-table-spec))
    (is (= (str/join "\n"
                     ["  OR   0   1  "
                      "----- --- ----"
                      "   0   0   1  "
                      "   1   1   1  "
                      ""])
           (nut/render fn-table-spec)))))
