(ns glossa.metazoa.provider.tutorial-test
  (:require
   [clojure.test :refer [deftest is]]
   [glossa.metazoa :as meta]
   [glossa.metazoa.api :as api]
   [glossa.metazoa.provider.tutorial :as nut]
   [glossa.metazoa.util :refer [eval-cleanroom]]
   [malli.core :as m]))

(def tut-0-intro
  {:before {:code '(do
                     (def testy (atom {:id (gensym)
                                       :coll [1 2 3]}))
                     (alter-meta! #'testy assoc :hi :there)
                     (:coll @testy))
            :expected [1 2 3]}
   :after {:code '(do
                    (swap! testy update :coll (partial map inc))
                    (:coll @testy))
           :expected [2 3 4]}
   :clean {:code '(let [ns (:ns (meta (resolve 'testy)))]
                    (ns-unmap ns 'testy)
                    (ns-resolve ns 'testy))
           :expected nil}
   :content
   [:div
    [:h1 "Tutorial Test!"]
    [:p "Metazoa is a tool for viewing, searching, query, and testing metadata."]]})

(def tut-1-eval
  {:before {:code '(do
                     (swap! testy update :coll (partial map (partial * 2)))
                     (:coll @testy))
            :expected [4 6 8]}
   :content
   [:div
    [:p "Evaluate the following at the REPL:"]
    [::meta/example
     [{:code '(map char (range 97 (+ 97 26)))
       :expected [\a \b \c \d \e \f \g
                  \h \i \j \k \l \m \n \o \p
                  \q \r \s \t \u \v \w \x \y \z]
       :ns *ns*}]]]})

(def tutorial-data
  {:author "Glossa Metazoa Maintainers"
   :ns *ns*
   :steps
   [[:intro tut-0-intro]
    [:eval tut-1-eval]]})

(def tutorial
  (nut/tutorial tutorial-data))

(deftest test-tutorial|valid
  (is (m/validate nut/schema tutorial-data))
  (is (m/validate nut/schema tutorial))
  (is (:ns tutorial) "The current namespace is set as the default if none provided.")
  (is (not (:title tutorial)) "The title of a tutorial is entirely optional."))

(def tutorial-data|invalid
  (dissoc tutorial-data :steps))

(deftest test-tutorial|invalid
  (is (thrown? clojure.lang.ExceptionInfo (nut/tutorial tutorial-data|invalid))))

(deftest test-tutorial|check
  (is (every? api/check-validator (nut/check tutorial))))

(deftest test-tutorial|check|throwable
  (let [tutorial (nut/tutorial {:ns *ns*
                                :steps [[:intro tut-0-intro]
                                        [:eval (assoc tut-1-eval :after {:code '(throw (RuntimeException. "Woohoo!"))})]]})]
    (is (every? api/check-validator (nut/check tutorial)))))

(deftest test-view|player-controls
  (is (= '[(|<|) (|toc|) (|>|)]
         (nut/view tutorial)))
  (nut/|exit|))

(deftest test-refresh
  (nut/view tutorial)
  (let [first-id (:id (deref (eval 'testy)))
        _ (nut/|refresh|)
        second-id (:id (deref (eval 'testy)))]
    (is (neg? (compare first-id second-id))))
  (nut/|exit|))

(deftest test-before-after-clean
  (let [tut-ns (:ns tutorial)]
    (nut/view tutorial)
    (is (= [1 2 3] (:coll @@(ns-resolve tut-ns 'testy))))
    (is (= '[(|<|) (|toc|) (|>|)]
           (eval '(|>|))))
    (is (= [4 6 8] (:coll @@(ns-resolve tut-ns 'testy)))
        "By this point, the :after of step 0 and the :before of step 1 should have evaluated.")
    (nut/|exit|)
    (is (nil? (ns-resolve tut-ns 'testy)))))

(defn- val-and-err
  [f]
  (let [{:keys [err result]} (eval-cleanroom *ns* f)]
    [result err]))

(def warn-msg (str @#'nut/warn-no-tutorial-msg "\n"))

(deftest test-warn-no-tutorial
  (nut/|exit|)
  (is (= [nil warn-msg] (val-and-err nut/|toc|)))
  (is (= [nil warn-msg] (val-and-err #(nut/|goto| :some-step))))
  (is (= [nil warn-msg] (val-and-err nut/|refresh|)))
  (is (= [nil warn-msg] (val-and-err nut/|<<|)))
  (is (= [nil warn-msg] (val-and-err nut/|>>|)))
  (is (= [nil warn-msg] (val-and-err nut/|<|)))
  (is (= [nil warn-msg] (val-and-err nut/|>|))))
