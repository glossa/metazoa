(ns glossa.metazoa.query.datascript-test
  (:require
   [clojure.test :refer [deftest is]]
   [glossa.metazoa.query.datascript :as nut]))

(alter-meta!
 #'-
 assoc
 :glossa.metazoa/fn-table
 {:left-hand-args (range -3 4)
  :right-hand-args (range -3 4)})

(deftest test-q
  (is (= #{['*default-data-reader-fn* "1.5"]
           ['as-> "1.5"]
           ['cond-> "1.5"]
           ['cond->> "1.5"]
           ['hash-ordered-coll "1.6"]
           ['hash-unordered-coll "1.6"]
           ['if-some "1.6"]
           ['mix-collection-hash "1.6"]
           ['record? "1.6"]
           ['reduced "1.5"]
           ['reduced? "1.5"]
           ['send-via "1.5"]
           ['set-agent-send-executor! "1.5"]
           ['set-agent-send-off-executor! "1.5"]
           ['some-> "1.5"]
           ['some->> "1.5"]
           ['some? "1.6"]
           ['unsigned-bit-shift-right "1.6"]
           ['when-some "1.6"]}

         (nut/q '[:find ?name ?added
                  :in $ ?ns [?added ...]
                  :where
                  [?e :ns ?ns]
                  [?e :name ?name]
                  [?e :added ?added]]
                (nut/db)
                (the-ns 'clojure.core)
                ["1.5" "1.6"]))))

(deftest test-q-custom-metadata
  (is (= #{[(the-ns 'clojure.core) '-]}
         (nut/q '[:find ?ns ?name
                  :in $ ?ns ?name
                  :where
                  [?e :glossa.metazoa/fn-table _]
                  [?e :ns ?ns]
                  [?e :name ?name]]
                (nut/db)
                (the-ns 'clojure.core)
                '-))))

(deftest test-clj-kondo-analysis-data
  (is (> (count (nut/q '[:find ?ns ?name
                         :in $ ?this
                         :where
                         [?callee :imeta/this ?this]
                         [?var-usage :var-usage/ref-to-var ?callee]
                         [?var-usage :var-usage/ref-from-var ?caller]
                         [?caller :ns ?ns]
                         [?caller :name ?name]]
                       (nut/db) #'map))
         3)))
