(ns glossa.metazoa-test
  (:require
   [clojure.test :refer [deftest is]]
   [glossa.metazoa :as nut]
   [glossa.metazoa.api :as api]
   [glossa.metazoa.provider.tutorial-test :as tutorial-test]
   [malli.core :as m]))

(nut/reset-query)
(nut/reset-search)

(deftest test-metadata
  (nut/test-imetas))

(deftest test-nut
  (nut/test-imeta (the-ns 'glossa.metazoa)))

(defn triple
  {::nut/doc [:h1 "n x 3"]
   ::nut/example
   [{:label "First Triple Example"
     :code '(triple 3)
     :expected 9
     :ns *ns*}
    {:label "Second Triple Example"
     :code '(map triple (range 1 4))
     :expected [3 6 9]
     :ns *ns*}]}
  [n]
  (* 3 n))

(def quad-example
  {:label "Quad Example"
   :code '(do
            (println "Light side!")
            (binding [*out* *err*] (println "Dark side!"))
            (map quad [1 2 3]))
   :expected [4 8 12]
   :expected-out "Light side!\n"
   :expected-err "Dark side!\n"
   :ns *ns*})

(defn quad
  {:elsewhere (nut/example quad-example)}
  [n]
  (* 4 n))

(def expected-quad-output
  (str "\n"
       ";; Quad Example\n"
       "(do\n"
       "  (println \"Light side!\")\n"
       "  (binding [*out* *err*] (println \"Dark side!\"))\n"
       "  (map quad [1 2 3]))\n"
       ";; [out] Light side!\n"
       ";; [err] Dark side!\n"
       "#_=> (4 8 12)"
       "\n"))

(deftest test-method-dispatch
  (is (= expected-quad-output
         (with-out-str (nut/view #'quad :elsewhere))))
  (is (= expected-quad-output
         (with-out-str (nut/view (nut/example quad-example))))))

(def check-valid? (m/validator api/schema-check))

(deftest test-check|example
  (let [checks (nut/check #'triple)]
    (is (= {::nut/example
            [{:actual 9
              :expected 9
              :code '(triple 3)
              :actual-out ""
              :actual-err ""}
             {:actual [3 6 9]
              :expected [3 6 9]
              :code '(map triple (range 1 4))
              :actual-out ""
              :actual-err ""}]
            ::nut/doc []}
           checks))
    (is (every? check-valid? (:glossa.metazoa/example checks))
        (map #(m/explain api/schema-check %) checks))))

(deftest test-check|specific-provider
  (let [rng (range 1 10)
        imeta (nut/imeta {::nut/fn-table
                          {:left-hand-args rng
                           :right-hand-args rng
                           :function mod}})
        [{:keys [actual]} :as chks] (nut/check imeta ::nut/fn-table)]
    (is (= 1 (count chks)))
    (is (and (string? actual)
             (> (count actual) 400)))))

(deftest test-check|standalone-provider-value
  (is (= [{:code
           '(do
              (println "Light side!")
              (binding [*out* *err*] (println "Dark side!"))
              (map quad [1 2 3])),
           :expected [4 8 12],
           :actual-out "Light side!\n",
           :expected-out "Light side!\n",
           :actual-err "Dark side!\n",
           :expected-err "Dark side!\n",
           :actual [4 8 12]}]
         (nut/check (nut/example quad-example)))))

(deftest test-check|example|with-output
  (let [checks (nut/check #'quad)]
    (is (= {:elsewhere
            [{:expected [4 8 12]
              :actual [4 8 12]
              :expected-err "Dark side!\n"
              :actual-err "Dark side!\n"
              :expected-out "Light side!\n"
              :actual-out "Light side!\n"
              :code (get quad-example :code)}]}
           checks))
    (is (every? check-valid? (:elsewhere checks))
        (map #(m/explain api/schema-check %) checks))))

(deftest test-test-imeta|provider|example
  (nut/test-imeta #'triple)
  (nut/test-imeta #'quad))

(deftest test-test-imeta|provider|specific-metadata-key
  (nut/test-imeta #'quad :elsewhere))

(def tut-var nil)
(alter-meta!
 #'tut-var
 assoc
 ::nut/tutorial
 tutorial-test/tutorial)

(deftest test-test-imeta|provider|tutorial
  (nut/test-imeta #'tut-var))

(deftest test-view|example
  (is (= "\n;; # n x 3 #\n\n"
         (with-out-str (nut/view #'triple))))
  (is (= (str "\n"
              ";; First Triple Example\n(triple 3)\n#_=> 9\n\n"
              ";; Second Triple Example\n(map triple (range 1 4))\n#_=> (3 6 9)"
              "\n")
         (with-out-str (nut/view #'triple ::nut/example)))))

(deftest test-view|example|with-output
  (is (= expected-quad-output
         (with-out-str (nut/view #'quad :elsewhere))))
  (is (= expected-quad-output
         (with-out-str (nut/view (nut/example quad-example))))))

(deftest test-providers
  (is (= [::nut/doc ::nut/example] (nut/providers #'triple))))

(deftest test-search
  (is (every? #(instance? clojure.lang.IMeta %) (nut/search "source")))
  (is (= (nut/search "source")
         (nut/search 'source)))
  (is (thrown-with-msg? clojure.lang.ExceptionInfo
                        #"Invalid search query"
                        (nut/search 42)))
  (is (not= (nut/search "source")
            (nut/search "name:source AND macro:true")))
  (is (> (count (nut/search "name:source AND -macro"))
         (count (nut/search "name:source AND macro:true"))))
  (is (seq (nut/search "ns:clojure.core AND imeta-value-type:java.lang.String")))
  (is (>= (count (nut/search "source")) 2))
  (is (= 1 (count (nut/search {:query "source" :num-hits 1}))))
  (is (= (nut/search {:query "source" :num-hits 1})
         (nut/search {:query "source" :limit 1}))))
